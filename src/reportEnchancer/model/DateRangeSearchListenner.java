package reportEnchancer.model;

import java.time.LocalDate;
import java.util.Date;

public interface DateRangeSearchListenner {
    void onSearchClicked(String column, LocalDate from, LocalDate to);
}
