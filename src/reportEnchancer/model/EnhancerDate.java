package reportEnchancer.model;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EnhancerDate extends Date {
    public EnhancerDate(long date) {
        super(date);
    }

    @Override
    public String toString(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(this);

        if (this.compareTo(new Date(0)) == 0){
            return "";
        }
        else if( (cal.get(Calendar.HOUR) == 0) && (cal.get(Calendar.MINUTE) == 0) && (cal.get(Calendar.SECOND) == 0) ){
            return new SimpleDateFormat("dd.MM.yyyy").format(this);
        }
        else {
            return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(this);
        }
    }
}
