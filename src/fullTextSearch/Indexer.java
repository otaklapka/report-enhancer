package fullTextSearch;

import java.io.*;
import java.nio.file.Paths;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class Indexer {

    private IndexWriter writer;

    /**
     *
     * @param indexDirectoryPath
     * @throws IOException
     */
    public Indexer(String indexDirectoryPath) throws IOException {
        Directory indexDirectory =
                FSDirectory.open(Paths.get(indexDirectoryPath));

        StandardAnalyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        writer = new IndexWriter(indexDirectory, iwc);
    }

    /**
     *
     * @throws CorruptIndexException
     * @throws IOException
     */
    public void close() throws CorruptIndexException, IOException {
        writer.close();
    }

    /**
     *
     * @param file
     * @return
     * @throws IOException
     */
    private Document getDocument(File file) throws IOException {
        Document document = new Document();

        TextField contentField = new TextField(LuceneConstants.CONTENTS, new FileReader(file));
        TextField fileNameField = new TextField(LuceneConstants.FILE_NAME,
                file.getName(),TextField.Store.YES);
        TextField filePathField = new TextField(LuceneConstants.FILE_PATH,
                file.getCanonicalPath(),TextField.Store.YES);

        document.add(contentField);
        document.add(fileNameField);
        document.add(filePathField);

        return document;
    }

    /**
     *
     * @param file
     * @throws IOException
     * @throws InvalidFormatException
     */
    public void indexFile(File file) throws IOException, InvalidFormatException {
        System.out.println("Indexing "+file.getCanonicalPath());
        String name = file.getName();
        switch (name.substring(name.length() - 4).toLowerCase()){
            case "html":
            case ".txt":
            case "json":
            case ".csv":
            case ".eml":
                writer.addDocument(getDocument(file));
                break;
            case ".pdf":
                writer.addDocument(getPDFFile(file));
                break;
            case "docx":
                writer.addDocument(getDocxFile(file));
                break;
            default:
                break;
        }
    }

    /**
     *
     * @param file
     * @return
     * @throws IOException
     */
    private Document getPDFFile(final File file) throws IOException{
        Document document = new Document();

        String contents = "";
        PDDocument doc = null;
        doc = PDDocument.load(file);
        PDFTextStripper stripper = new PDFTextStripper();

        stripper.setLineSeparator("\n");
        stripper.setStartPage(1);
        stripper.setEndPage(50);
        contents = stripper.getText(doc);
        doc.close();

        TextField contentField = new TextField(LuceneConstants.CONTENTS, contents, Field.Store.YES);
        TextField fileNameField = new TextField(LuceneConstants.FILE_NAME,
                file.getName(),TextField.Store.YES);
        TextField filePathField = new TextField(LuceneConstants.FILE_PATH,
                file.getCanonicalPath(),TextField.Store.YES);

        document.add(contentField);
        document.add(fileNameField);
        document.add(filePathField);
        return document;
    }

    /**
     *
     * @param file
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    private Document getDocxFile(final File file) throws IOException, InvalidFormatException {
        Document document = new Document();
        FileInputStream fis = new FileInputStream(file);
        XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
        XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);

        TextField contentField = new TextField(LuceneConstants.CONTENTS, extractor.getText(), Field.Store.YES);
        TextField fileNameField = new TextField(LuceneConstants.FILE_NAME,
                file.getName(),TextField.Store.YES);
        TextField filePathField = new TextField(LuceneConstants.FILE_PATH,
                file.getCanonicalPath(),TextField.Store.YES);

        document.add(contentField);
        document.add(fileNameField);
        document.add(filePathField);
        return document;
    }

    /**
     *
     * @param dataDirPath
     * @param filter
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public int createIndex(String dataDirPath, FileFilter filter) throws IOException, InvalidFormatException {
        File[] files = new File(dataDirPath).listFiles();

        for (File file : files) {
            if(!file.isDirectory()
                    && !file.isHidden()
                    && file.exists()
                    && file.canRead()
                    && filter.accept(file)
            ){
                indexFile(file);
            }
        }
        return writer.numDocs();
    }
}
