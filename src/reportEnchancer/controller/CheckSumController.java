package reportEnchancer.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CheckSumController {
    @FXML
    private Label md5Label1;
    @FXML
    private Label md5Label2;
    @FXML
    private Label sha1Label1;
    @FXML
    private Label sha1Label2;
    @FXML
    private Label md5ResultLabel;
    @FXML
    private Label sha1ResultLabel;


    public static byte[] checksum(String filepath, MessageDigest md) throws IOException {

        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
        }

        return md.digest();

    }

    public static String bytesToHex(byte[] hashInBytes) {

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();

    }


    public void compare(String md5, String sha1, File file){

        md5Label1.setText(md5);
        sha1Label1.setText(sha1);

        try (InputStream is = Files.newInputStream(Paths.get(file.getPath()))) {
            String countedMD5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] hashInBytes = checksum(file.getPath(), md);

            md5Label2.setText(countedMD5);
            sha1Label2.setText(bytesToHex(hashInBytes));

            if(md5.equals(countedMD5)){
                md5ResultLabel.setText("Součty se shodují");
                md5ResultLabel.setTextFill(Color.web("#4caf50"));
            } else {
                md5ResultLabel.setText("Součty se neshodují");
                md5ResultLabel.setTextFill(Color.web("#f44336"));
            }

            if(sha1.equals(bytesToHex(hashInBytes))){
                sha1ResultLabel.setText("Součty se shodují");
                sha1ResultLabel.setTextFill(Color.web("#4caf50"));
            } else {
                sha1ResultLabel.setText("Součty se neshodují");
                sha1ResultLabel.setTextFill(Color.web("#f44336"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void close(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) md5Label1.getScene().getWindow();
            currentStage.close();
        });
    }
}
