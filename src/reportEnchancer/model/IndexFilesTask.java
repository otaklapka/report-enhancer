package reportEnchancer.model;

import fullTextSearch.AcceptableFileFilter;
import fullTextSearch.Indexer;
import fullTextSearch.LuceneConstants;
import javafx.concurrent.Task;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

public class IndexFilesTask extends Task<Void> {
    private File reportFilesFolder;
    private IndexTaskEventListenner delegate;
    private boolean clearIndexFolder = true;

    public IndexFilesTask(File reportFilesFolder, IndexTaskEventListenner delegate, boolean clearIndexFolder) {
        this.reportFilesFolder = reportFilesFolder;
        this.delegate = delegate;
        this.clearIndexFolder = clearIndexFolder;
    }

    @Override
    protected Void call() throws Exception {
        // full text index
        updateMessage("Odstraňování předešlých indexů.");
        if(clearIndexFolder) {
            try {
                File path = new File(Settings.prefs.get(Settings.INDEX_DIR, LuceneConstants.INDEX_FOLDER));
                if (path.exists()) {
                    FileUtils.forceDelete(path);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            updateMessage("Indexace souborů reportu.");
            Indexer indexer = new Indexer(Settings.prefs.get(Settings.INDEX_DIR, LuceneConstants.INDEX_FOLDER));




            AcceptableFileFilter filter = new AcceptableFileFilter();

            int i = 0;
            try {
                Collection files = FileUtils.listFiles(new File(reportFilesFolder.toString()), null, true);
                for (Iterator iterator = files.iterator(); iterator.hasNext(); ) {
                    File file = (File) iterator.next();
                    if (!file.isDirectory()
                            && !file.isHidden()
                            && file.exists()
                            && file.canRead()
                            && filter.accept(file)
                    ) {
                        indexer.indexFile(file);
                    }
                    i++;
                    updateMessage(String.format("Indexace souboru %s.", file.getName()));
                    this.updateProgress(i, files.size());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            indexer.close();
            delegate.onTaskDone();
        } catch (Exception ex){
            delegate.onExceptionThrown(ex);
            ex.printStackTrace();
        }
        return null;
    }
}
