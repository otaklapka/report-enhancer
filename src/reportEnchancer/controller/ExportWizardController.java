package reportEnchancer.controller;

import Database.FileRecord;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.stage.Stage;

import java.io.*;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ExportWizardController {
    @FXML
    private CheckBox includePictures;
    @FXML
    private CheckBox includeCaseInformation;
    @FXML
    private CheckBox includeFileOverview;
    @FXML
    private CheckBox includeEvidenceList;
    @FXML
    private CheckBox includeTexts;
    @FXML
    private CheckBox includeEmails;
    @FXML
    private Label titleLabel;

    private ArrayList<FileRecord> fileList = new ArrayList<FileRecord>();
    private String caseInformation, fileOverview, evidenceList = "";
    private ArrayList<TableColumn<FileRecord, ?>> columns = new ArrayList<TableColumn<FileRecord, ?>>();

    public void setFileList(ArrayList<FileRecord> fileList) {
        this.fileList = fileList;
        titleLabel.setText(String.format("Export %d souborů", fileList.size()));
    }

    public void setCaseInformation(String caseInformation) {
        this.caseInformation = caseInformation;
    }

    public void setFileOverview(String fileOverview) {
        this.fileOverview = fileOverview;
    }

    public void setEvidenceList(String evidenceList) {
        this.evidenceList = evidenceList;
    }

    public void setColumns(ArrayList<TableColumn<FileRecord, ?>> columns) {
        this.columns = columns;
    }

    public void export(ActionEvent actionEvent) throws Exception {
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder.append("<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'></head><body>");
        if (includeCaseInformation.isSelected()){
            stringBuilder.append(caseInformation);
        }
        if (includeEvidenceList.isSelected()){
            stringBuilder.append(evidenceList);
        }
        if(includeFileOverview.isSelected()){
            stringBuilder.append(fileOverview);
        }

        for (FileRecord file: fileList) {
            stringBuilder.append("<table><tbody>");

            for (TableColumn<FileRecord, ?> column : columns){
                try {
                    String getter = String.format("get%s%s", column.getId().substring(0,1).toUpperCase(), column.getId().substring(1));
                    System.out.println(getter);
                    Method fieldGetter = FileRecord.class.getMethod(getter);
                    String value = fieldGetter.invoke(file).toString();

                    stringBuilder.append(String.format("<tr><td>%s</td><td>%s</td></tr>", column.getText(), value));
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            if(file.getLocalPath().exists()){
                stringBuilder.append(String.format("<tr><td colspan='2'><a href='%s'>Otevřít</a></td></tr>", file.getLocalPath().getPath()));
            }

            if(file.isImage() && includePictures.isSelected() && file.getLocalPath().exists()){
                stringBuilder.append(String.format("<tr><td colspan=2><a href=\"%s\" target=\"_blank\"><img width='100' height='100' src='%s'></a></td></tr>", file.getLocalPath(), file.getLocalPath()));
            }

            String value = file.getLocalPath().getPath();
            if(file.getLocalPath().exists()) {
                if ((file.isEmail() && includeEmails.isSelected()) || (file.isText() && includeTexts.isSelected())) {
                    try {
                        value = new String(Files.readAllBytes(Paths.get(file.getLocalPath().getPath())), StandardCharsets.UTF_8);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                value = "Soubor není přiložen.";
            }
            stringBuilder.append(String.format("<tr><td colspan=2>%s</td></tr>",value));
            stringBuilder.append("<tbody></table><br>");
        }
        stringBuilder.append("<script>window.print();</script></body></html>");
        PrintWriter writer = new PrintWriter("export.html", "UTF-8");
        writer.print(stringBuilder.toString());
        writer.close();
        MainController.openFile(new File("export.html"));

        final Node source = (Node) actionEvent.getSource();
        final Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    public void stornoClicked(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) titleLabel.getScene().getWindow();
            currentStage.close();
        });
    }
}