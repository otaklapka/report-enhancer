package ReportParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLReportParser {
    public   static final String NAME = "Name";
    public  final static String P_SIZE = "Physical Size";
    public  final static String L_SIZE = "Logical Size";
    public  final static String CREATED = "Created Date";
    public  final static String MODIFIED = "Modified Date";
    public  final static String ACCESSED = "Accessed Date";
    public  final static String PATH = "Path";
    public  final static String EXPORTED = "Exported as";
    public  final static String SEPARATOR = "File Comments:";
    public  final static String LABEL = "Label";
    public  final static String ITEM_NUMBER = "Item Number";
    public  final static String EXTENSION = "Extension";
    public  final static String FILE_TYPE = "File Type";
    public  final static String MD5 = "MD5 Hash";
    public  final static String SHA1 = "SHA1 Hash";
    public  final static String CARVED = "Carved";
    public  final static String DELETED = "Deleted";
    public  final static String AUTHOR = "Author";
    public  final static String LAST_SAVED_BY = "Last Saved By";
    public  final static String EMAIL_SUBMIT_TIME = "Submit Time";
    public  final static String EMAIL_DELIVERY_TIME = "Delivery Time";
    public  final static String COMMENT = SEPARATOR;

    public static final String CREATE_TIME = "Create Time";
    public static final String EMBEDDED_COMMENTS = "Embedded Comments";
    public static final String HIDDEN_COLUMNS_OR_ROWS = "Hidden Columns or Rows";
    public static final String HIDDEN_WORKSHEETS = "Hidden Worksheets";
    public static final String LAST_PRINTED = "Last Printed";
    public static final String LAST_SAVED_TIME = "Last Saved Time";
    public static final String REVISION_NUMBER = "Revision Number";
    public static final String TOTAL_EDITING_TIME = "Total Editing Time";
    public static final String TRACK_CHANGES = "Track Changes";
    public static final String EMAIL_SUBJECT = "Subject";
    public static final String EMAIL_TO = "To";
    public static final String EMAIL_FROM = "From";
    public static final String EMAIL_CC = "CC";
    public static final String EMAIL_BCC = "BCC";
    public static final String EMAIL_UNREAD = "Unread";
    public static final String EMAIL_UNSENT = "Unsent";
    public static final String EMAIL_HAS_ATTACHMENT = "Has Attachment";
    public static final String EMAIL_PRIORITY = "Priority";
    public static final String EMAIL = "Email";
    public static final String EMAIL_SRC = "Src";

    public  final static String CASE_INFO_FILE = "CaseInfo.html";
    public  final static String EVIDENCE_LIST_FILE = "EvidenceList.html";
    public  final static String FILE_OVERVIEW_FILE = "FileOverview.html";

    /**
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String parseCaseInfo(File dir) throws IOException {
        final File caseInfo =  new File(dir.getPath() + File.separator + CASE_INFO_FILE);
        String caseInfoContent = new String(Files.readAllBytes(Paths.get(caseInfo.getPath())), StandardCharsets.UTF_8);
        Document caseInfoDoc = Jsoup.parse(caseInfoContent);
        caseInfoDoc.select("img").remove();
        return caseInfoDoc.toString();
    }

    /**
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String parseFileOverview(File dir) throws IOException {
        final File fileOverview =  new File(dir.getPath() + File.separator + FILE_OVERVIEW_FILE);
        String fileOverviewContent = new String(Files.readAllBytes(Paths.get(fileOverview.getPath())), StandardCharsets.UTF_8);
        Document fileOverviewDoc = Jsoup.parse(fileOverviewContent);
        fileOverviewDoc.select("img").remove();
        return fileOverviewDoc.toString();
    }

    /**
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static String parseEvidenceList(File dir) throws IOException {
        final File evidenceList =  new File(dir.getPath() + File.separator + EVIDENCE_LIST_FILE);
        String evidenceListContent = new String(Files.readAllBytes(Paths.get(evidenceList.getPath())), StandardCharsets.UTF_8);
        Document evidenceListDoc = Jsoup.parse(evidenceListContent);
        evidenceListDoc.select("img").remove();
        return evidenceListContent.toString();
    }


    /**
     *
     * @param dir
     * @return
     * @throws IOException
     */
    public static Report parse(File dir) throws IOException {
        return new Report(parseCaseInfo(dir), parseFileOverview(dir), parseEvidenceList(dir), parseBookmarks(dir));
    }

    /**
     *
     * @param s
     * @return
     */
    private static int extractNumber(String s) {
        Pattern numberPattern = Pattern.compile("\\d*");
        Matcher m = numberPattern.matcher(s);
        if(m.find()) {
            try{
                return Integer.parseInt(m.group(0));
            } catch (Exception ex){
                //ex.printStackTrace();
                return 0;
            }
        }
        else {
            return 0;
        }
    }

    /**
     *
     * @param sceFiles
     * @return
     * @throws IOException
     */
    private static ArrayList<ParsedFile> parseFiles(ArrayList<File> sceFiles) throws IOException {
        // souhrny seznam radku ze vsech stranek bookmarku
        Elements elements = new Elements();
        ArrayList<ParsedFile> files = new ArrayList<ParsedFile>();

        // vytvoreni souhrneho seznamu radku ze vsech stranek bookmarku
        for (File file : sceFiles)
        {
            // cteni obsahu stranky zalozky
            String contents = new String(Files.readAllBytes(Paths.get(file.getPath())), StandardCharsets.UTF_8);
            Document doc = Jsoup.parse(contents);
            elements.addAll(doc.select("div.row, div.clrBkgrnd.bkmkSeparator.bkmkValue"));
        }

        ParsedFile parsedFile = new ParsedFile();
        parsedFile.comment = elements.get(0).child(0).text().replace("File Comments:", "");
        elements.remove(0);

        String datePattern = "dd.MM.yyyy";
        String dateTimePattern = "dd.MM.yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(dateTimePattern);

        for (Element e : elements){
            // nazev radku Name, Item Number ...
            String id = e.child(0).text().trim();

            if(id.equals(SEPARATOR)){
                parsedFile.comment = id.replace("File Comments:", "");
                files.add(parsedFile);
                parsedFile = new ParsedFile();
            }else {
                // hodnota
                String text = e.child(1).text().trim();
                switch (id){
                    case NAME:
                        parsedFile.name = text;
                        break;
                    case P_SIZE:
                        parsedFile.pSize = extractNumber(text);
                        break;
                    case L_SIZE:
                        parsedFile.lSize = extractNumber(text);
                        break;
                    case CREATED:
                        try {
                            parsedFile.created = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case MODIFIED:
                        try {
                            parsedFile.modified = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case ACCESSED:
                        try {
                            parsedFile.accessed = simpleDateFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case PATH:
                        parsedFile.path = text;
                        break;
                    case EXPORTED:
                        parsedFile.exported = text;
                    case LABEL:
                        parsedFile.label = text;
                        break;
                    case ITEM_NUMBER:
                        parsedFile.itemNumber = extractNumber(text);
                        break;
                    case EXTENSION:
                        parsedFile.ftkExtension = text;
                        break;
                    case FILE_TYPE:
                        parsedFile.fileType = text;
                        break;
                    case MD5:
                        parsedFile.MD5 = text;
                        break;
                    case SHA1:
                        parsedFile.SHA1 = text;
                        break;
                    case CARVED:
                        parsedFile.carved = text.trim().equals("True");
                        break;
                    case DELETED:
                        parsedFile.deleted = text.trim().equals("True");
                        break;
                    case AUTHOR:
                        parsedFile.author = text;
                        break;
                    case LAST_SAVED_BY:
                        parsedFile.lastSavedBy = text;
                        break;
                    case EMAIL_SUBMIT_TIME:
                        try {
                            parsedFile.emailSubmitTime = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case EMAIL_DELIVERY_TIME:
                        try {
                            parsedFile.emailDeliveryTime = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case CREATE_TIME:
                        try {
                            parsedFile.createTime = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case EMBEDDED_COMMENTS:
                        parsedFile.embeddedComments = text;
                        break;
                    case HIDDEN_COLUMNS_OR_ROWS:
                        parsedFile.hiddenColumnsOrRows = text;
                        break;
                    case HIDDEN_WORKSHEETS:
                        parsedFile.hiddenWorksheets = text;
                        break;
                    case LAST_PRINTED:
                        try {
                            parsedFile.lastPrinted = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case LAST_SAVED_TIME:
                        try {
                            parsedFile.lastSavedTime = simpleDateTimeFormat.parse(text);
                        } catch (ParseException ex){ ex.printStackTrace(); }
                        break;
                    case REVISION_NUMBER:
                        parsedFile.revisionNumber = extractNumber(text);
                        break;
                    case TOTAL_EDITING_TIME:
                        parsedFile.totalEditingTime = text;
                        break;
                    case TRACK_CHANGES:
                        parsedFile.trackChanges = text;
                        break;
                    case EMAIL_SUBJECT:
                        parsedFile.emailSubject = text;
                        break;
                    case EMAIL_TO:
                        parsedFile.emailTo = text;
                        break;
                    case EMAIL_FROM:
                        parsedFile.emailFrom = text;
                        break;
                    case EMAIL_CC:
                        parsedFile.emailCC = text;
                        break;
                    case EMAIL_BCC:
                        parsedFile.emailBCC = text;
                        break;
                    case EMAIL_UNREAD:
                        parsedFile.emailUnread = text;
                        break;
                    case EMAIL_UNSENT:
                        parsedFile.emailUnsent = text;
                        break;
                    case EMAIL_HAS_ATTACHMENT:
                        parsedFile.emailHasAttachment = text;
                        break;
                    case EMAIL_PRIORITY:
                        parsedFile.emailPriority = text;
                        break;
                    case EMAIL:
                        parsedFile.email = text;
                        break;
                    case EMAIL_SRC:
                        parsedFile.emailSrc = text;
                        break;
                    default:
                        break;
                }
            }
        }
        return files;
    }

    /**
     *
     * @param sce
     * @return
     * @throws IOException
     */
    private static ArrayList<Bookmark> parseBookmarks(File sce) throws IOException {
        // ziskani cesty k souboru
        File bmkFile = new File(sce.getPath() + File.separator + "Bookmarks.html");
        // cteni
        String contents = new String(Files.readAllBytes(Paths.get(bmkFile.getPath())), StandardCharsets.UTF_8);
        // ulozeni do objektu
        Document doc = Jsoup.parse(contents);

        // ziskani odkazu na prvni stranku
        Elements rows = doc.select("body > table:nth-child(5) tr td a");

        ArrayList<Bookmark> bookmarks = new ArrayList<Bookmark>();
        for (Element e: rows) {
            // otevreni souboru
            File fstBmk = new File(sce.getPath() + File.separator + e.attr("href"));
            // cteni
            contents = new String(Files.readAllBytes(Paths.get(fstBmk.getPath())),StandardCharsets.UTF_8);
            // ulozeni do objektu
            doc = Jsoup.parse(contents);
            // nalezeni odkazu na posledni soubor <a href="Bookmark_bk_ID4001(8).html">Last Page&gt;&gt;&gt;&gt;</a>
            Element lastBmkE = doc.selectFirst("body > table:nth-child(2) > tbody > tr > td:nth-child(4) > a");


            int lastBmkIdx = 1;
            // kontrola jestli existuje vice stranek
            if(lastBmkE != null) {
                // ziskani cisla posledni stranky
                Pattern p = Pattern.compile("\\((\\d*?)\\)");
                Matcher m = p.matcher(lastBmkE.attr("href"));
                m.find();
                lastBmkIdx = Integer.parseInt(m.group(1));
            }
            ArrayList<File> bmkFiles = new ArrayList<File>();
            // generovani nazvu souboru bookmarku
            for(int i = 1; i <= lastBmkIdx; i++){
                String fileName = e.attr("href").replace("(1)", String.format("(%d)", i));
                File file = new File(sce.getPath() + File.separator + fileName);
                if(file.exists()){
                    bmkFiles.add(file);
                }
            }
            bookmarks.add(new Bookmark(e.text(), bmkFiles, parseFiles(bmkFiles)));
        }
        return bookmarks;
    }
}
