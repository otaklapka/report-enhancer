package ReportParser;

import java.util.ArrayList;

public class Report {
    private String caseInfoHTML;
    private String fileOverviewHTML;
    private String evidenceListHTML;
    private ArrayList<Bookmark> bookmarks;

    public String getCaseInfoHTML() {
        return caseInfoHTML;
    }

    public String getFileOverviewHTML() {
        return fileOverviewHTML;
    }

    public String getEvidenceListHTML() {
        return evidenceListHTML;
    }

    public ArrayList<Bookmark> getBookmarks() {
        return bookmarks;
    }

    /**
     *
     * @param caseInfoHTML
     * @param fileOverviewHTML
     * @param evidenceListHTML
     * @param bookmarks
     */
    public Report(String caseInfoHTML, String fileOverviewHTML, String evidenceListHTML, ArrayList<Bookmark> bookmarks) {
        this.caseInfoHTML = caseInfoHTML;
        this.fileOverviewHTML = fileOverviewHTML;
        this.evidenceListHTML = evidenceListHTML;
        this.bookmarks = bookmarks;
    }
}
