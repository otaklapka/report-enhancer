package fullTextSearch;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class AcceptableFileFilter implements FileFilter {
    private ArrayList<String> allowedExtensions;

    public AcceptableFileFilter() {
        allowedExtensions = new ArrayList<String>();
        allowedExtensions.add("html");
        allowedExtensions.add("json");
        allowedExtensions.add(".csv");
        allowedExtensions.add(".pdf");
        allowedExtensions.add("docx");
        allowedExtensions.add(".eml");
        allowedExtensions.add(".txt");
    }

    /**
     *
     * @param pathname
     * @return
     */
    @Override
    public boolean accept(File pathname) {
        String name = pathname.getName();

        return name.length() > 4 && allowedExtensions.contains(name.substring(name.length() - 4).toLowerCase());
    }
}