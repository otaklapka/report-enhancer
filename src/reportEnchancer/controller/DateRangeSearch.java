package reportEnchancer.controller;

import Database.Query;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import javafx.util.Pair;
import reportEnchancer.model.DateRangeSearchListenner;

class Column {
    private String label;
    private String column;

    public Column(String label, String column) {
        this.label = label;
        this.column = column;
    }

    @Override
    public String toString() {
        return label;
    }

    public String getLabel() {
        return label;
    }

    public String getColumn() {
        return column;
    }
}

public class DateRangeSearch {
    private DateRangeSearchListenner delegate = null;

    @FXML
    DatePicker fromDatePicker;
    @FXML
    DatePicker toDatePicker;
    @FXML
    ComboBox columnCombobox;

    public void initialize(){
        ObservableList<Column> l = FXCollections.observableArrayList();
        l.add(new Column("Poslední přístup", Query.ACCESSED_DATE));
        l.add(new Column("Naposledy upraveno", Query.UPDATED_DATE));
        l.add(new Column("Vytvořeno", Query.CREATED_DATE));
        l.add(new Column("Doručení emailu", Query.EMAIL_DELIVERY_TIME));
        l.add(new Column("Odeslání emailu", Query.EMAIL_SUBMIT_TIME));
        columnCombobox.setItems(l);
        columnCombobox.getSelectionModel().select(0);
    }

    public void submitListenner(DateRangeSearchListenner delegate) {
        this.delegate = delegate;
    }

    public void stornoClicked(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) columnCombobox.getScene().getWindow();
            currentStage.close();
        });
    }

    public void searchClicked(ActionEvent actionEvent) {
        if(delegate != null){
            delegate.onSearchClicked(((Column) columnCombobox.getSelectionModel().getSelectedItem()).getColumn(), fromDatePicker.getValue(), toDatePicker.getValue());
        }
        this.stornoClicked(new ActionEvent());
    }
}
