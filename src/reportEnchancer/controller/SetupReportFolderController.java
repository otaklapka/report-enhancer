package reportEnchancer.controller;

import Database.Query;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import reportEnchancer.model.CSVParseTask;
import reportEnchancer.model.ParseReportTaskListenner;
import reportEnchancer.model.HTMLParseTask;
import reportEnchancer.model.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SetupReportFolderController implements ParseReportTaskListenner {
    public Button indexDirButton;
    @FXML
    private Button openSourceFolderButton;
    @FXML
    private Button openSourceFileButton;
    @FXML
    private Label sourceFolderPathLabel;
    @FXML
    private Label sourceFilePathLabel;
    @FXML
    private ProgressBar windowProgressBar;
    @FXML
    private Label windowProgressBarLabel;
    @FXML
    private VBox windowInfoArea;
    @FXML
    private CheckBox indexFilesCheckBox;
    @FXML
    private RadioButton HTMLRadio;
    @FXML
    private RadioButton csvRadio;
    @FXML
    private RadioButton dbRadio;
    @FXML
    private Label indexDirLabel;

    private DirectoryChooser directoryChooser = new DirectoryChooser();
    private FileChooser fileChooser = new FileChooser();
    private File reportFilesDir = null;
    private File indexDir = null;
    private File sourceFile = null;
    private Task parseTask;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public SetupReportFolderController(){
        // nastaveni plochy jako defaultní složky ve file dialogu - pokud existuje
        String userDirectoryString = String.format("%s%sDesktop",  System.getProperty("user.home"), File.separator);
        File userDirectory = new File(userDirectoryString);
        directoryChooser.setInitialDirectory(userDirectory);
        directoryChooser.setTitle("Zvolte složku");
        fileChooser.setInitialDirectory(userDirectory);
        fileChooser.setTitle("Zvolte zdrojový soubor");
    }

    public void handleOpenIndexDir(ActionEvent actionEvent) {
        indexDir = directoryChooser.showDialog(openSourceFolderButton.getScene().getWindow());
        if (indexDir != null) {
            indexDirLabel.setText(indexDir.getPath());
            Settings.prefs.put(Settings.INDEX_DIR, indexDir.getPath());
        }
    }

    public void handleOpenSourceFolder() {
        // přidání dialogu do okna
        reportFilesDir = directoryChooser.showDialog(openSourceFolderButton.getScene().getWindow());
        if (reportFilesDir != null) {
            sourceFolderPathLabel.setText(reportFilesDir.getPath());
        }
    }

    public void handleOpenSourceFile(ActionEvent actionEvent) {
        fileChooser.getExtensionFilters().clear();
        if(csvRadio.isSelected()){
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        }
        else if (dbRadio.isSelected()) {
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("SQLITE3", "*.sqlite3"));
        }

        sourceFile = fileChooser.showOpenDialog(openSourceFolderButton.getScene().getWindow());
        if (sourceFile != null){
            sourceFilePathLabel.setText(sourceFile.getPath());
        }
    }

    @FXML
    private void runParser(){

        if(HTMLRadio.isSelected()) {
            if (reportFilesDir != null) {
                parseTask = new HTMLParseTask(reportFilesDir, this);
            } else {
                // jestli byl dialog zavřen bez vybrání souboru, zobrazí se zpráva a dál se nepokračuje
                return;
            }
        } else if(csvRadio.isSelected()) {
            if(sourceFile != null){
                parseTask = new CSVParseTask(sourceFile, reportFilesDir, this, true);
            } else {
                return;
            }
        } else if (dbRadio.isSelected()){
            if(sourceFile != null){
                Query.CON_URL = sourceFile.getPath();
                onTaskDone("", "", "");
                return;
            } else {
                return;
            }
        }

        // navázání komponent na task aby byl schopný měnit jejich hodnoty
        windowProgressBarLabel.textProperty().bind(parseTask.messageProperty());
        windowProgressBar.progressProperty().bind(parseTask.progressProperty());
        windowProgressBar.visibleProperty().bind(parseTask.runningProperty());
        windowInfoArea.visibleProperty().bind(parseTask.runningProperty());
        // spuštění parsování v samostatném vláknu, jinak by aplikace zamrzla
        executor.submit(parseTask);
    }

    @Override
    public void onTaskDone(String caseInformationHTML, String fileOverviewHTML, String evidenceListHTML) {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) openSourceFolderButton.getScene().getWindow();
            currentStage.close();
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/main.fxml"));
                Parent root = loader.load();

                MainController controller = (MainController) loader.getController();
                controller.setCaseInformation(caseInformationHTML);
                controller.setFileOverview(fileOverviewHTML);
                controller.setEvidenceList(evidenceListHTML);
                controller.setReportFilesDir(reportFilesDir);
                controller.setFilesIndexed(indexFilesCheckBox.isSelected() || indexDir != null);

                Stage stage = new Stage();
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent t) {
                        Platform.exit();
                        System.exit(0);
                    }
                });
                stage.setTitle("Report enhancer");
                stage.setScene(new Scene(root, 800, 480));
                stage.setMaximized(true);
                stage.show();

                if(indexFilesCheckBox.isSelected()){
                    loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/IndexFiles.fxml"));
                    root = loader.load();
                    IndexFilesController indexerController = loader.getController();
                    Stage indexStage = new Stage();
                    indexStage.setTitle("Indexace...");
                    indexStage.setScene(new Scene(root, 300, 100));


                    if(reportFilesDir != null) {
                        indexStage.show();
                        indexerController.start(reportFilesDir);
                    }
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onExceptionThrown(Exception e) {
        Platform.runLater(() -> {
            String msg = HTMLRadio.isSelected() ? "Při načítání HTML reportu došlo k chybě. Zkontrolujte zda vkládáte správný adresář Report_Files a že se jedná o HTML report s jednou úrovní záložek.\n" : "Během načítání reportu došlo k neočekávané chybě.\n";
            MainController.showAlertDialog("Chyba", msg);
        });
    }

    @Override
    public void fileNotFoundException(FileNotFoundException e) {
        Platform.runLater(() -> {
            MainController.showAlertDialog("Chyba", "Některé soubory nebyly nalezeny.\nZkontrolujte zda jste zvolili správný adresář.");
        });
    }

    public void stornoClicked(ActionEvent actionEvent) {
        if(parseTask != null){
            try {
                parseTask.cancel();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        Platform.runLater(() -> {
            Stage currentStage = (Stage) openSourceFolderButton.getScene().getWindow();
            currentStage.close();
        });
        System.exit(0);
    }

    public void radioSelected(ActionEvent actionEvent) {
        openSourceFileButton.setDisable(false);
        sourceFolderPathLabel.setText("Zvolte  adresář se soubory exportu...");
        sourceFilePathLabel.setText("Zdrojový soubor nevybrán..." );

        if(HTMLRadio.isSelected()) {
            openSourceFileButton.setDisable(true);
            sourceFolderPathLabel.setText("Zvolte adresář Report_files...");
        }
        else if (csvRadio.isSelected()) {
            sourceFilePathLabel.setText("Zvolte CSV soubor...");
        }
        else if (dbRadio.isSelected()){
            sourceFilePathLabel.setText("Zvolte soubor databáze...");
        }
    }

    public void indexFilesCheckBoxToggled(ActionEvent actionEvent) {
        if(indexFilesCheckBox.isSelected()){
            indexDirButton.setDisable(true);
            indexDir = null;
        } else {
            indexDirButton.setDisable(false);
        }
    }
}
