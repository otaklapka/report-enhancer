package reportEnchancer.controller;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class BrowserController {
    @FXML
    private WebView browser;
    private WebEngine engine;

    @FXML
    public void initialize() {
        engine = browser.getEngine();
    }

    public void load(String HTML){
        engine.loadContent(HTML);
    }
}
