package ReportParser;

import java.io.File;
import java.util.ArrayList;

public class Bookmark {
    private String name;
    private ArrayList<File> files;
    private ArrayList<ParsedFile> parsedFiles;

    /**
     *
     * @param name
     * @param files
     * @param parsedFiles
     */
    public Bookmark(String name, ArrayList<File> files, ArrayList<ParsedFile> parsedFiles) {
        this.name = name;
        this.files = files;
        this.parsedFiles = parsedFiles;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public ArrayList<File> getFiles() {
        return files;
    }

    /**
     *
     * @return
     */
    public ArrayList<ParsedFile> getParsedFiles() {
        return parsedFiles;
    }
}
