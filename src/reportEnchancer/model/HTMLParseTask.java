package reportEnchancer.model;

import ReportParser.Bookmark;
import ReportParser.ParsedFile;
import ReportParser.Report;
import ReportParser.HTMLReportParser;
import javafx.concurrent.Task;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class HTMLParseTask extends ParseTask {

    private File reportFilesFolder;
    private ParseReportTaskListenner listenner;

    public HTMLParseTask(File reportFilesFolder, ParseReportTaskListenner listenner) {
        this.reportFilesFolder = reportFilesFolder;
        this.listenner = listenner;
    }

    @Override public Void call() {
        String caseInformationHTML = "";
        String fileOverviewHTML = "";
        String evidenceListHTML = "";
        ArrayList<String> extensions = new ArrayList<String>();
        try {
            Report report = HTMLReportParser.parse(reportFilesFolder);

            try {
                this.prepareDBStructure();
            } catch (SQLException e){
                e.printStackTrace();
                listenner.onExceptionThrown(e);
                return null;
            }

            updateMessage("Převádění dat ze zdrojového souboru do databáze.");
            Connection dbConnection = this.getDbConnection();
            if (dbConnection == null) {
                return null;
            }

            dbConnection.setAutoCommit(false);
            PreparedStatement filePrepStmnt = null, extPrepStmnt = null, bookmPrepStmnt = null, filePathsPrepStmnt = null;

            try {
                filePrepStmnt = dbConnection.prepareStatement(INSERT_FILE_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                bookmPrepStmnt = dbConnection.prepareStatement(INSERT_BOOKMARK_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                extPrepStmnt = dbConnection.prepareStatement(INSERT_EXTENSION_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                filePathsPrepStmnt = dbConnection.prepareStatement(INSERT_FILE_PATHS_QUERY,
                        Statement.RETURN_GENERATED_KEYS);

                int bookmarkID = 1;
                for (Bookmark b : report.getBookmarks()){
                    updateMessage(String.format("Parsování %s", b.getName()));

                    bookmPrepStmnt.setInt(1, bookmarkID);
                    bookmPrepStmnt.setString(2, b.getName());

                    int bookmRowsAffected = bookmPrepStmnt.executeUpdate();
                    if (bookmRowsAffected != 1) {
                        updateMessage("Vyskytla se chyba při vkládání záložky.");
                        dbConnection.rollback();
                        return null;
                    }

                    int processedFile = 0;
                    for (ParsedFile file : b.getParsedFiles()) {

                        filePathsPrepStmnt.setString(1, file.name);
                        filePathsPrepStmnt.setString(2, reportFilesFolder.toString() + File.separator + "files" + File.separator + file.name);

                        int rowsAffected = filePathsPrepStmnt.executeUpdate();
                        if (rowsAffected != 1) {
                            listenner.onExceptionThrown(new Exception("FilePath insert error"));
                            dbConnection.rollback();
                            return null;
                        }

                        int extensionsID = extensions.indexOf(file.getExtension());
                        if(extensionsID == -1){
                            extensionsID = extensions.size();
                            extensions.add(file.getExtension());

                            extPrepStmnt.setInt(1, extensionsID);
                            extPrepStmnt.setString(2, file.getExtension());

                            int extRowsAffected = extPrepStmnt.executeUpdate();
                            if (extRowsAffected != 1) {
                                updateMessage("Vyskytla se chyba při vkládání přípony.");
                                dbConnection.rollback();
                                return null;
                            }
                        }

                        filePrepStmnt.setString(1, file.comment);
                        filePrepStmnt.setString(2, file.name);
                        filePrepStmnt.setString(3, file.ftkExtension);
                        filePrepStmnt.setInt(4, extensionsID);
                        filePrepStmnt.setInt(5, bookmarkID);
                        filePrepStmnt.setLong(6, file.pSize);
                        filePrepStmnt.setLong(7, file.lSize);
                        filePrepStmnt.setDate(8, new Date(file.created.getTime()));
                        filePrepStmnt.setDate(9, new Date(file.modified.getTime()));
                        filePrepStmnt.setDate(10, new Date(file.emailSubmitTime.getTime()));
                        filePrepStmnt.setDate(11, new Date(file.emailDeliveryTime.getTime()));
                        filePrepStmnt.setDate(12, new Date(file.accessed.getTime()));
                        filePrepStmnt.setString(13, file.label);
                        filePrepStmnt.setInt(14, file.itemNumber);
                        filePrepStmnt.setString(15, file.fileType);
                        filePrepStmnt.setString(16, file.MD5);
                        filePrepStmnt.setString(17, file.SHA1);
                        filePrepStmnt.setBoolean(18, file.carved);
                        filePrepStmnt.setBoolean(19, file.deleted);
                        filePrepStmnt.setString(20, file.author);
                        filePrepStmnt.setString(21, file.lastSavedBy);
                        filePrepStmnt.setString(22, file.exported);
                        filePrepStmnt.setString(23, file.path);
                        filePrepStmnt.setDate(24, new Date(file.createTime.getTime()));
                        filePrepStmnt.setString(25, file.embeddedComments);
                        filePrepStmnt.setString(26, file.hiddenColumnsOrRows);
                        filePrepStmnt.setString(27, file.hiddenWorksheets);
                        filePrepStmnt.setDate(28, new Date(file.lastPrinted.getTime()));
                        filePrepStmnt.setDate(29, new Date(file.lastSavedTime.getTime()));
                        filePrepStmnt.setInt(30, file.revisionNumber);
                        filePrepStmnt.setString(31, file.totalEditingTime);
                        filePrepStmnt.setString(32, file.trackChanges);
                        filePrepStmnt.setString(33, file.emailSubject);
                        filePrepStmnt.setString(34, file.emailTo);
                        filePrepStmnt.setString(35, file.emailFrom);
                        filePrepStmnt.setString(36, file.emailCC);
                        filePrepStmnt.setString(37, file.emailBCC);
                        filePrepStmnt.setString(38, file.emailUnread);
                        filePrepStmnt.setString(39, file.emailUnsent);
                        filePrepStmnt.setString(40, file.emailHasAttachment);
                        filePrepStmnt.setString(41, file.emailPriority);
                        filePrepStmnt.setString(42, file.email);
                        filePrepStmnt.setString(43, file.emailSrc);

                        int fileRowsAffected = filePrepStmnt.executeUpdate();
                        if (fileRowsAffected != 1) {
                            updateMessage("Vyskytla se chyba při vkládání.");
                            dbConnection.rollback();
                            return null;
                        }

                        this.updateProgress(processedFile, b.getParsedFiles().size());
                        processedFile ++;
                    }
                    bookmarkID ++;
                }
                dbConnection.commit();
            } catch (SQLException e) {
                dbConnection.rollback();
                System.out.println(e.getMessage());
                System.out.println("Rollbacking...");
                updateMessage("Vyskytla se chyba při vkládání.");
                listenner.onExceptionThrown(e);
            } finally {
                try {
                    if (filePrepStmnt != null) {
                        filePrepStmnt.close();
                    }
                    if (extPrepStmnt != null) {
                        extPrepStmnt.close();
                    }
                    dbConnection.close();
                } catch (SQLException e3) {
                    System.out.println(e3.getMessage());
                }
                updateMessage("Databáze byla vytvořena.");
            }

            updateMessage("Načítání přehledů.");
            caseInformationHTML = report.getCaseInfoHTML();
            fileOverviewHTML = report.getFileOverviewHTML();
            evidenceListHTML = report.getEvidenceListHTML();
            listenner.onTaskDone(caseInformationHTML, fileOverviewHTML, evidenceListHTML);
        }
        catch (FileNotFoundException e){
            updateMessage("Soubor nenalezen.");
            e.printStackTrace();
            listenner.fileNotFoundException(e);
        }
        catch (Exception e) {
            updateMessage("Neočekávaná chyba.");
            e.printStackTrace();
            listenner.onExceptionThrown(e);
        }
        return null;
    }
}