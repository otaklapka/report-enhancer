package reportEnchancer.controller;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import reportEnchancer.model.CSVParseTask;
import reportEnchancer.model.ParseReportTaskListenner;
import reportEnchancer.model.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.Executors;

public class CSVImportController implements ParseReportTaskListenner {
    @FXML
    public Label csvLabel;
    @FXML
    public Label dirLabel;
    @FXML
    public Label pregressbarLabel;
    @FXML
    public ProgressBar progressbar;
    @FXML
    public Button importButton;
    @FXML
    public CheckBox indexCheckbox;

    private DirectoryChooser directoryChooser = new DirectoryChooser();
    private FileChooser fileChooser = new FileChooser();
    private File csv = null;
    private File dir = null;

    public CSVImportController() {
        String userDirectoryString = String.format("%s%sDesktop", System.getProperty("user.home"), File.separator);
        File userDirectory = new File(userDirectoryString);
        directoryChooser.setInitialDirectory(userDirectory);
        fileChooser.setInitialDirectory(userDirectory);
    }

    public void importClick(ActionEvent actionEvent) {
        if (csv != null) {
            Task t = new CSVParseTask(csv, dir, this, false);
            pregressbarLabel.textProperty().bind(t.messageProperty());
            progressbar.progressProperty().bind(t.progressProperty());
            Executors.newSingleThreadExecutor().submit(t);
        }
    }

    public void stornoClick(ActionEvent actionEvent) {
        close();
    }

    public void openCSVClick(ActionEvent actionEvent) {
        csv = fileChooser.showOpenDialog(csvLabel.getScene().getWindow());
        if (csv != null) {
            csvLabel.setText(csv.getPath());
            importButton.setDisable(false);
        }
    }

    public void openDirClick(ActionEvent actionEvent) {
        dir = directoryChooser.showDialog(csvLabel.getScene().getWindow());
        if (dir != null) {
            dirLabel.setText(dir.getPath());
            indexCheckbox.setDisable(false);
        }
    }

    public void indexToggle(ActionEvent actionEvent) {

    }

    private void close() {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) importButton.getScene().getWindow();
            currentStage.close();
        });
    }

    @Override
    public void onTaskDone(String caseInformationHTML, String fileOverviewHTML, String evidenceListHTML) {
        close();
        if (indexCheckbox.isSelected() && dir != null) {
            Platform.runLater(() -> {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/IndexFiles.fxml"));
                    Parent root = loader.load();
                    IndexFilesController indexerController = loader.getController();
                    Stage indexStage = new Stage();
                    indexStage.setTitle("Indexace...");
                    indexStage.setScene(new Scene(root, 300, 100));
                    indexStage.show();
                    indexerController.setClearIndexDir(false);
                    indexerController.start(dir);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onExceptionThrown(Exception e) {

    }

    @Override
    public void fileNotFoundException(FileNotFoundException e) {

    }
}
