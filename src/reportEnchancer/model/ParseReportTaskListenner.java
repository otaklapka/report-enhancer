package reportEnchancer.model;

import java.io.FileNotFoundException;

public interface ParseReportTaskListenner {
    void onTaskDone(String caseInformationHTML, String fileOverviewHTML, String evidenceListHTML);
    void onExceptionThrown(Exception e);
    void fileNotFoundException(FileNotFoundException e);
}
