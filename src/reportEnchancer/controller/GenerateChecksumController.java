package reportEnchancer.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static reportEnchancer.controller.CheckSumController.bytesToHex;
import static reportEnchancer.controller.CheckSumController.checksum;

public class GenerateChecksumController {
    public Label filePathLabel;
    public Button generateChecksumButton;
    private FileChooser fileChooser = new FileChooser();
    private File file = null;

    public void stornoClicked(ActionEvent actionEvent) {
        close();
    }

    public void generateChecksum(ActionEvent actionEvent) throws NoSuchAlgorithmException {
        try (InputStream is = Files.newInputStream(Paths.get(file.getPath()))) {
            String countedMD5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);

            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] sha1Bytes = checksum(file.getPath(), md);

            writeFile(String.format("%s%schecksum_MD5.txt", file.getParent(), File.separator), countedMD5);
            writeFile(String.format("%s%schecksum_SHA1.txt", file.getParent(), File.separator), bytesToHex(sha1Bytes));

            close();
        } catch (IOException e){
            e.printStackTrace();
            MainController.showAlertDialog("Chyba", "Nepodařilo se vygenerovat sumu");
        }
    }

    private void writeFile(String path, String content) throws IOException {
        FileWriter fileWriter = new FileWriter(path);
        fileWriter.write(content);
        fileWriter.close();
    }

    public void openFile(ActionEvent actionEvent) {
        file = fileChooser.showOpenDialog(filePathLabel.getScene().getWindow());
        if (file != null){
            filePathLabel.setText(file.getPath());
            generateChecksumButton.setDisable(false);
        }
    }

    private void close() {
        Platform.runLater(() -> {
            Stage currentStage = (Stage) filePathLabel.getScene().getWindow();
            currentStage.close();
        });
    }
}
