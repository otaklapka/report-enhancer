package reportEnchancer.controller;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import reportEnchancer.model.IndexTaskEventListenner;
import reportEnchancer.model.ParseReportTaskListenner;
import reportEnchancer.model.IndexFilesTask;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IndexFilesController implements IndexTaskEventListenner {
    @FXML
    private Label label;
    @FXML
    private ProgressBar progressBar;

    private Task indexTask = null;
    private ParseReportTaskListenner delegate;
    private boolean clearIndexDir = true;

    public void start(File path){
        ExecutorService executor = Executors.newSingleThreadExecutor();
        indexTask = new IndexFilesTask(path, this, clearIndexDir);
        executor.submit(indexTask);
        label.textProperty().bind(indexTask.messageProperty());
        progressBar.progressProperty().bind(indexTask.progressProperty());
    }

    public void stornoClicked(ActionEvent actionEvent) {
        if(indexTask != null){
            try {
                indexTask.cancel();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        Platform.runLater(() -> {
            Stage currentStage = (Stage) progressBar.getScene().getWindow();
            currentStage.close();
        });
    }

    @Override
    public void onTaskDone() {
        this.stornoClicked(new ActionEvent());
    }

    @Override
    public void onExceptionThrown(Exception e) {
        MainController.showAlertDialog("Chyba indexace", "Při indexaci došlo k chybě.");
        this.stornoClicked(new ActionEvent());
    }

    public boolean isClearIndexDir() {
        return clearIndexDir;
    }

    public void setClearIndexDir(boolean clearIndexDir) {
        this.clearIndexDir = clearIndexDir;
    }
}
