package reportEnchancer.model;

public interface IndexTaskEventListenner {
    void onTaskDone();
    void onExceptionThrown(Exception e);
}
