package ReportParser;

import java.util.Date;

public class ParsedFile {
    public static final String NO_EXTENSION = "Bez přípony";

    public String name = "";
    public long pSize = 0;
    public long lSize = 0;
    public Date created = new Date(0);
    public Date modified = new Date(0);
    public Date accessed = new Date(0);
    public String path = "";
    public String exported = "";
    public String comment = "";
    public String label = "";
    public int itemNumber = 0;
    public String ftkExtension = "";
    public String fileType = "";
    public String MD5 = "";
    public String SHA1 = "";
    public boolean carved = false;
    public boolean deleted = false;
    public String author = "";
    public String lastSavedBy = "";
    public Date emailSubmitTime = new Date(0);
    public Date emailDeliveryTime = new Date(0);

    public Date createTime = new Date(0);
    public String embeddedComments = "";
    public String hiddenColumnsOrRows = "";
    public String hiddenWorksheets = "";
    public Date lastPrinted = new Date(0);
    public Date lastSavedTime = new Date(0);
    public int revisionNumber = 0;
    public String totalEditingTime = "";
    public String trackChanges = "";
    public String emailSubject = "";
    public String emailTo = "";
    public String emailFrom = "";
    public String emailCC = "";
    public String emailBCC = "";
    public String emailUnread = "";
    public String emailUnsent = "";
    public String emailHasAttachment = "";
    public String emailPriority = "";
    public String email = "";
    public String emailSrc = "";

    /**
     *
     * @param name
     * @return
     */
    public static String getExtension(String name){
        int pos = name.lastIndexOf('.');
        if(pos != -1){
            String afterDot = name.substring(pos +1);
            if(afterDot.length() < 5){
                return afterDot.toLowerCase();
            }
        }
        return NO_EXTENSION;
    }

    /**
     *
     * @return
     */
    public String getExtension(){
        return getExtension(name);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                "Physical Size: %s\n" +
                "Logical Size: %s\n" +
                "Created Date: %s\n" +
                "Modified Date: %s\n" +
                "Accessed Date: %s\n" +
                "Path: %s\n" +
                "Exported as: %s\n" +
                "File Comments: %s\n", name, pSize, lSize, created, modified, accessed, path, exported, comment);
    }
}