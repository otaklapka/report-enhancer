package reportEnchancer.model;

import Database.Query;
import ReportParser.ParsedFile;
import javafx.util.Pair;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class CSVParseTask extends ParseTask {
    public static final String NAME = "Name";
    public final static String P_SIZE = "P-Size (bytes)";
    public final static String L_SIZE = "L-Size (bytes)";
    public final static String CREATED = "Created";
    public final static String MODIFIED = "Modified";
    public final static String ACCESSED = "Accessed";
    public final static String PATH = "Path";
    public final static String EXPORTED = "Exported as";
    public final static String SEPARATOR = "File Comments:";
    public final static String LABEL = "Label";
    public final static String ITEM_NUMBER = "Item #";
    public final static String EXTENSION = "Ext";
    public final static String FILE_TYPE = "File Type";
    public final static String MD5 = "MD5";
    public final static String SHA1 = "SHA1";
    public final static String CARVED = "Carved";
    public final static String DELETED = "Deleted";
    public final static String AUTHOR = "Author";
    public final static String LAST_SAVED_BY = "Last Saved By";
    public final static String EMAIL_SUBMIT_TIME = "Submit Time";
    public final static String EMAIL_DELIVERY_TIME = "Delivery Time";
    public final static String COMMENT = SEPARATOR;

    public static final String CREATE_TIME = "Create Time";
    public static final String EMBEDDED_COMMENTS = "Embedded Comments";
    public static final String HIDDEN_COLUMNS_OR_ROWS = "Hidden Columns or Rows";
    public static final String HIDDEN_WORKSHEETS = "Hidden Worksheets";
    public static final String LAST_PRINTED = "Last Printed";
    public static final String LAST_SAVED_TIME = "Last Saved Time";
    public static final String REVISION_NUMBER = "Revision Number";
    public static final String TOTAL_EDITING_TIME = "Total Editing Time";
    public static final String TRACK_CHANGES = "Track Changes";
    public static final String EMAIL_SUBJECT = "Subject";
    public static final String EMAIL_TO = "To";
    public static final String EMAIL_FROM = "From";
    public static final String EMAIL_CC = "CC";
    public static final String EMAIL_BCC = "BCC";
    public static final String EMAIL_UNREAD = "Unread";
    public static final String EMAIL_UNSENT = "Unsent";
    public static final String EMAIL_HAS_ATTACHMENT = "Has Attachment";
    public static final String EMAIL_PRIORITY = "Priority";
    public static final String EMAIL = "Email";
    public static final String EMAIL_SRC = "Src";
    public static final String CATEGORY = "Category";

    private File csv;
    private File dir;
    private ParseReportTaskListenner listenner;
    private final String notAttached = "Soubor není přiložen";
    private boolean resetDatabase = true;
    private Query query = new Query();

    public CSVParseTask(File csv, File dataDir, ParseReportTaskListenner listenner, boolean resetDatabase) {
        this.csv = csv;
        this.dir = dataDir;
        this.listenner = listenner;
        this.resetDatabase = resetDatabase;
    }

    @Override
    public Void call() {
        HashMap<String, Integer> extensions = new HashMap<String, Integer>();
        try {

            if(resetDatabase) {
                try {
                    this.prepareDBStructure();
                } catch (SQLException e) {
                    e.printStackTrace();
                    listenner.onExceptionThrown(e);
                    return null;
                }
            } else {
                ArrayList<Pair<Integer, String>> l =  query
                        .reset()
                        .table(Query.TABLE_EXTENSIONS)
                        .orderBy(Query.ID)
                        .getExtensions();

                for (Pair<Integer, String> ex : l){
                   extensions.put(ex.getValue(), ex.getKey());
                }
            }

            updateMessage("Převádění dat ze zdrojového souboru do databáze.");
            Connection dbConnection = this.getDbConnection();
            if (dbConnection == null) {
                listenner.onExceptionThrown(new Exception("Connection was null"));
                return null;
            }

            dbConnection.setAutoCommit(false);
            PreparedStatement filePrepStmnt = null, extPrepStmnt = null, bookmPrepStmnt = null, filePathsPrepStmnt = null;

            try {
                filePrepStmnt = dbConnection.prepareStatement(INSERT_FILE_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                bookmPrepStmnt = dbConnection.prepareStatement(INSERT_BOOKMARK_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                extPrepStmnt = dbConnection.prepareStatement(INSERT_EXTENSION_QUERY,
                        Statement.RETURN_GENERATED_KEYS);
                filePathsPrepStmnt = dbConnection.prepareStatement(INSERT_FILE_PATHS_QUERY,
                        Statement.RETURN_GENERATED_KEYS);

                int bookmarkID = 1;

                if(!resetDatabase) {
                    ArrayList<Pair<Integer, String>> l = query
                            .reset()
                            .table(Query.TABLE_BOOKMARKS)
                            .orderBy(Query.ID)
                            .getBookmarks();

                    bookmarkID = l.size() +1;
                }
                bookmPrepStmnt.setInt(1, bookmarkID);
                bookmPrepStmnt.setString(2, csv.getName());

                int bookmRowsAffected = bookmPrepStmnt.executeUpdate();
                if (bookmRowsAffected != 1) {
                    listenner.onExceptionThrown(new Exception("Bookmark insert error"));
                    dbConnection.rollback();
                    return null;
                }


                if (dir != null) {
                    updateMessage("Skenování složky se soubory.");
                    try {
                        boolean recursive = true;
                        Collection files = FileUtils.listFiles(dir, null, recursive);

                        int c = 0;
                        for (Iterator iterator = files.iterator(); iterator.hasNext(); ) {
                            File file = (File) iterator.next();
                            filePathsPrepStmnt.setString(1, file.getName());
                            filePathsPrepStmnt.setString(2, file.getAbsolutePath());
                            int rowsAffected = filePathsPrepStmnt.executeUpdate();
                            if (rowsAffected != 1) {
                                listenner.onExceptionThrown(new Exception("FilePath insert error"));
                                dbConnection.rollback();
                                return null;
                            }
                            this.updateProgress(c, files.size());
                            c++;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                updateMessage("Výpočet řádků souboru.");
                Path path = Paths.get(csv.getPath());
                long lineCount = Files.lines(path).count();


                long startTime = System.currentTimeMillis();
                int secElapsed = 0;
                int minElapsed = 0;
                LineIterator li = FileUtils.lineIterator(csv, "UTF-8");
                int lineCounter = 0;
                ArrayList<String> headers = new ArrayList<String>();
                try {
                    while (li.hasNext()) {
                        String line = li.nextLine();
                        if (lineCounter == 0) {
                            headers.addAll(Arrays.asList(line.split(",")));
                        } else {
                            String[] s = line.split(",");
                            HashMap<String, String> record = new HashMap<String, String>();
                            for (int y = 0; y < s.length; y++) {
                                if (y < headers.size()) {
                                    record.put(headers.get(y), s[y]);
                                }
                            }

                            if (!record.containsKey(NAME)) {
                                continue;
                            }

                            secElapsed = ((int) (((System.currentTimeMillis() - startTime) / 1000)) - minElapsed *60);
                            minElapsed = secElapsed > 59 ? minElapsed+=1 : minElapsed;

                            updateMessage(String.format("%02d min %02d s Ukládání souboru %d z %d: %s", minElapsed, secElapsed, lineCounter, lineCount, record.get(NAME)));

                            String ext = ParsedFile.getExtension(record.get(NAME));
                            int extensionsID = extensions.getOrDefault(ext, -1);
                            if (extensionsID == -1) {

                                for (Map.Entry<String, Integer> e : extensions.entrySet()){
                                    extensionsID = e.getValue() > extensionsID ? e.getValue() : extensionsID;
                                }

                                extensionsID++;
                                extensions.put(ext, extensionsID);

                                extPrepStmnt.setInt(1, extensionsID);
                                extPrepStmnt.setString(2, ext);

                                int extRowsAffected = extPrepStmnt.executeUpdate();
                                if (extRowsAffected != 1) {
                                    dbConnection.rollback();
                                    listenner.onExceptionThrown(new Exception("Extension insert error"));
                                    return null;
                                }
                            }

                            filePrepStmnt.setString(1, record.getOrDefault(COMMENT, ""));
                            filePrepStmnt.setString(2, record.getOrDefault(NAME, ""));
                            filePrepStmnt.setString(3, record.getOrDefault(EXTENSION, ""));
                            filePrepStmnt.setInt(4, extensionsID);
                            filePrepStmnt.setInt(5, bookmarkID);
                            filePrepStmnt.setLong(6, record.containsKey(P_SIZE) ? parseInt(record.get(P_SIZE)) : 0);
                            filePrepStmnt.setLong(7, record.containsKey(L_SIZE) ? parseInt(record.get(L_SIZE)) : 0);
                            filePrepStmnt.setDate(8, record.containsKey(CREATED) ? parseDate(record.get(CREATED)) : new Date(0));
                            filePrepStmnt.setDate(9, record.containsKey(MODIFIED) ? parseDate(record.get(MODIFIED)) : new Date(0));
                            filePrepStmnt.setDate(10, record.containsKey(EMAIL_SUBMIT_TIME) ? parseDate(record.get(EMAIL_SUBMIT_TIME)) : new Date(0));
                            filePrepStmnt.setDate(11, record.containsKey(EMAIL_DELIVERY_TIME) ? parseDate(record.get(EMAIL_DELIVERY_TIME)) : new Date(0));
                            filePrepStmnt.setDate(12, record.containsKey(ACCESSED) ? parseDate(record.get(ACCESSED)) : new Date(0));
                            filePrepStmnt.setString(13, record.getOrDefault(LABEL, ""));
                            filePrepStmnt.setInt(14, record.containsKey(ITEM_NUMBER) ? parseInt(record.get(ITEM_NUMBER)) : 0);
                            filePrepStmnt.setString(15, record.getOrDefault(FILE_TYPE, ""));
                            filePrepStmnt.setString(16, record.getOrDefault(MD5, ""));
                            filePrepStmnt.setString(17, record.getOrDefault(SHA1, ""));
                            filePrepStmnt.setBoolean(18, record.containsKey(CARVED) && record.get(CARVED).equals("TRUE"));
                            filePrepStmnt.setBoolean(19, record.containsKey(DELETED) && record.get(DELETED).equals("TRUE"));
                            filePrepStmnt.setString(20, record.getOrDefault(AUTHOR, ""));
                            filePrepStmnt.setString(21, record.getOrDefault(LAST_SAVED_BY, ""));
                            filePrepStmnt.setString(22, record.getOrDefault(PATH, ""));
                            filePrepStmnt.setString(23, record.getOrDefault(EXPORTED, ""));

                            filePrepStmnt.setDate(24, record.containsKey(CREATE_TIME) ? parseDate(record.get(CREATE_TIME)) : new Date(0));
                            filePrepStmnt.setString(25, record.getOrDefault(EMBEDDED_COMMENTS, ""));
                            filePrepStmnt.setString(26, record.getOrDefault(HIDDEN_COLUMNS_OR_ROWS, ""));
                            filePrepStmnt.setString(27, record.getOrDefault(HIDDEN_WORKSHEETS, ""));
                            filePrepStmnt.setDate(28, record.containsKey(LAST_PRINTED) ? parseDate(record.get(LAST_PRINTED)) : new Date(0));
                            filePrepStmnt.setDate(29, record.containsKey(LAST_SAVED_TIME) ? parseDate(record.get(LAST_SAVED_TIME)) : new Date(0));
                            filePrepStmnt.setInt(30, record.containsKey(REVISION_NUMBER) ? parseInt(record.get(REVISION_NUMBER)) : 0);
                            filePrepStmnt.setString(31, record.getOrDefault(TOTAL_EDITING_TIME, ""));
                            filePrepStmnt.setString(32, record.getOrDefault(TRACK_CHANGES, ""));
                            filePrepStmnt.setString(33, record.getOrDefault(EMAIL_SUBJECT, ""));
                            filePrepStmnt.setString(34, record.getOrDefault(EMAIL_TO, ""));
                            filePrepStmnt.setString(35, record.getOrDefault(EMAIL_FROM, ""));
                            filePrepStmnt.setString(36, record.getOrDefault(EMAIL_CC, ""));
                            filePrepStmnt.setString(37, record.getOrDefault(EMAIL_BCC, ""));
                            filePrepStmnt.setString(38, record.getOrDefault(EMAIL_UNREAD, ""));
                            filePrepStmnt.setString(39, record.getOrDefault(EMAIL_UNSENT, ""));
                            filePrepStmnt.setString(40, record.getOrDefault(EMAIL_HAS_ATTACHMENT, ""));
                            filePrepStmnt.setString(41, record.getOrDefault(EMAIL_PRIORITY, ""));
                            filePrepStmnt.setString(42, record.getOrDefault(EMAIL, ""));
                            filePrepStmnt.setString(43, record.getOrDefault(EMAIL_SRC, ""));
                            filePrepStmnt.setString(44, record.getOrDefault(CATEGORY, ""));


                            int fileRowsAffected = filePrepStmnt.executeUpdate();
                            if (fileRowsAffected != 1) {
                                updateMessage("Vyskytla se chyba při vkládání.");
                                dbConnection.rollback();
                                return null;
                            }

                            this.updateProgress(lineCounter, lineCount);
                        }
                        lineCounter++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    li.close();
                }
                dbConnection.commit();
            } catch (SQLException e) {
                dbConnection.rollback();
                System.out.println(e.getMessage());
                System.out.println("Rollbacking...");
                updateMessage("Vyskytla se chyba při vkládání.");
                listenner.onExceptionThrown(e);
            } finally {
                try {
                    if (filePrepStmnt != null) {
                        filePrepStmnt.close();
                    }
                    if (extPrepStmnt != null) {
                        extPrepStmnt.close();
                    }
                    if (bookmPrepStmnt != null) {
                        bookmPrepStmnt.close();
                    }
                    if (filePathsPrepStmnt != null) {
                        filePathsPrepStmnt.close();
                    }
                    dbConnection.close();
                } catch (SQLException e3) {
                    System.out.println(e3.getMessage());
                }
                updateMessage("Databáze byla vytvořena.");
            }

            updateMessage("Načítání přehledů.");
            listenner.onTaskDone(notAttached, notAttached, notAttached);
        } catch (
                FileNotFoundException e) {
            updateMessage("Soubor nenalezen.");
            e.printStackTrace();
            listenner.fileNotFoundException(e);
        } catch (Exception e) {
            updateMessage("Neočekávaná chyba.");
            e.printStackTrace();
            listenner.onExceptionThrown(e);
        }

        return null;
    }
}
