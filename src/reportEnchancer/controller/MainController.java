package reportEnchancer.controller;

import Database.FileRecord;
import Database.Query;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import reportEnchancer.model.DateRangeSearchListenner;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.*;

public class MainController implements DateRangeSearchListenner {
    @FXML
    public TextField tableSearchText;
    @FXML
    public VBox progressIndicator;
    @FXML
    public Label progressMessage;
    @FXML
    private ListView<String> windowExtensionsListView;
    @FXML
    private ListView<String> windowBookmarksListView;
    @FXML
    private TableView<FileRecord> windowFilesTableView;
    @FXML
    private Pagination tablePagination;
    @FXML
    private TableColumn<FileRecord, Boolean> checkBoxTableColumn;

    private String caseInformation, fileOverview, evidenceList = "";
    private File reportFilesDir;
    private ArrayList<FileRecord> selectedFiles = new ArrayList<FileRecord>();
    private ArrayList<Pair<Integer, String>> bookmarks;
    private ArrayList<Pair<Integer, String>> extensions;
    private String selectedBookmark = "";
    private String selectedExtension = "";
    private ArrayList<TableColumn<FileRecord, ?>> tableColumns;
    private final int LIMIT = 500;
    private final int PAGE_LIMIT = 50;

    private Query query = new Query().limit(LIMIT);

    public void setFilesIndexed(boolean filesIndexed) {
        isFilesIndexed = filesIndexed;
    }

    private boolean isFilesIndexed = false;

    @FXML
    public void initialize() {
        tableColumns = new ArrayList<TableColumn<FileRecord, ?>>(windowFilesTableView.getColumns());
        // checkbox init
        checkBoxTableColumn.setCellFactory(CheckBoxTableCell.forTableColumn(new Callback<Integer, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(Integer index) {

                ObservableValue<Boolean> itemBoolean = windowFilesTableView.getItems().get(index).checkedProperty();
                itemBoolean.addListener(change -> {
                    FileRecord item = windowFilesTableView.getItems().get(index);

                    if(item.checkedProperty().get() && !selectedFiles.contains(item)){
                        selectedFiles.add(item);
                    }
                    if(!item.checkedProperty().get()){
                        selectedFiles.removeIf(f -> f.getID() == item.getID());
                    }
                });
                return itemBoolean;
           }
        }));

        tablePagination.setStyle("-fx-page-information-visible: false;");

        initBookmarksExtensions();
    }

    private void initBookmarksExtensions() {
        progressIndicator.setVisible(true);
        Executors.newSingleThreadExecutor().submit(() -> {

            extensions = query
                    .reset()
                    .orderBy(Query.NAME)
                    .order(Query.ORDER.ASC)
                    .limit(LIMIT)
                    .table(Query.TABLE_EXTENSIONS)
                    .getExtensions();

            bookmarks = query
                    .reset()
                    .limit(LIMIT)
                    .table(Query.TABLE_BOOKMARKS)
                    .getBookmarks();

            final ObservableList<String> extCol = FXCollections.observableArrayList();
            for (Pair<Integer, String> extension : extensions) {
                extCol.add(extension.getValue());
            }

            Platform.runLater(() -> {
                this.windowExtensionsListView.setItems(extCol);
            });

            final ObservableList<String> bookmCol = FXCollections.observableArrayList();
            for (Pair<Integer, String> bookmark : bookmarks) {
                bookmCol.add(bookmark.getValue());
            }

            Platform.runLater(() -> {
                this.windowBookmarksListView.setItems(bookmCol);
            });

            ArrayList<TableColumn<FileRecord, ?>> columns = new ArrayList<TableColumn<FileRecord, ?>>();
            Executors.newSingleThreadExecutor().submit(() -> {
                query.table(Query.TABLE_FILES);

                for (TableColumn<FileRecord, ?> column : tableColumns) {
                    String dbColumnName = FileRecord.mapToDBCols.get(column.getId());

                    if (dbColumnName != null) {
                        if (FileRecord.numericColumns.contains(dbColumnName)) {
                            query
                                    .where(dbColumnName, ">", 0);
                        } else {
                            query
                                    .where(dbColumnName, "<>", "");
                        }
                    }

                    Platform.runLater(() -> {
                        progressMessage.setText(String.format("Kontrola dat v sloupci %s", column.getText()));
                    });

                    int count = query.getFiles().size();
                    query.removeWhere(dbColumnName);
                    if (count > 0) {
                        columns.add(column);
                    }
                }

                Platform.runLater(() -> {
                    windowFilesTableView.getColumns().setAll(columns);
                    bindOnSort();
                    syncFileTableWithDBManager();
                    this.tablePagination.setVisible(true);
                    this.tablePagination.setPageCount(query.reset().getCount() / PAGE_LIMIT);
                    this.tablePagination.setPageFactory(this::createTablePage);
                    this.windowExtensionsListView.setVisible(true);
                    progressIndicator.setVisible(false);
                    progressMessage.setText("");
                });
            });
        });
    }

    private void bindOnSort(){
        windowFilesTableView.setOnSort(sortEvent -> {
            ObservableList<TableColumn<FileRecord, ?>> sortOrder = windowFilesTableView.getSortOrder();
            if(sortOrder.size() > 0){
                TableColumn column = windowFilesTableView.getSortOrder().get(0);
                TableColumn.SortType sortType = column.getSortType();

                String dbColumnName = FileRecord.mapToDBCols.get(column.getId());

                query.orderBy(dbColumnName);

                if(sortType == TableColumn.SortType.DESCENDING){
                    query.order(Query.ORDER.DESC);
                } else {
                    query.order(Query.ORDER.ASC);
                }
            } else {
                // reset
                query.orderBy(Query.ID);
                query.order(Query.ORDER.DESC);
            }
            syncFileTableWithDBManager();
        });
    }

    public static void showAlertDialog(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    private Node createTablePage(int page) {
        query.offset(page * PAGE_LIMIT);
        query.limit(PAGE_LIMIT);
        this.syncFileTableWithDBManager();
        return new BorderPane(this.windowFilesTableView);
    }

    private void syncFileTableWithDBManager() {
        showLoading();
        progressMessage.setText("Načítání dat...");

        ArrayList<FileRecord> l = new ArrayList<FileRecord>();
        Executors.newSingleThreadExecutor().submit(() -> {
            int pages = (int) Math.ceil((double) query.clone().offset(0).getCount() / PAGE_LIMIT);

            for (FileRecord f : query.getFiles()){
                if (isSelected(f)){
                    f.setChecked(true);
                }
                l.add(f);
            }

            Platform.runLater(() -> {
                windowFilesTableView.getItems().setAll(l);
                this.tablePagination.setPageCount(pages);
                hideLoading();
                progressMessage.setText("");
            });
        });
    }

    private boolean isSelected(FileRecord f){
        return  selectedFiles.stream().filter(o -> o.getID() == f.getID()).findFirst().isPresent();
    }

    private void showLoading(){
        tablePagination.setVisible(false);
        progressIndicator.setVisible(true);
    }

    private void hideLoading(){
        tablePagination.setVisible(true);
        progressIndicator.setVisible(false);
    }

    private void runSearch(){
        String searchText = this.tableSearchText.getText();
        query
                .reset()
                .limit(LIMIT)
                .like(Query.NAME, searchText);
        this.syncFileTableWithDBManager();
        Platform.runLater(() -> {
                    this.tablePagination.setPageCount((int) Math.ceil((double) query.getCount() / PAGE_LIMIT));
                }
        );
    }

    @FXML
    public void handleSearch(ActionEvent actionEvent) {
        runSearch();
    }
    
    @FXML
    public void fileSelected(MouseEvent mouseEvent) {
        if (mouseEvent.isPrimaryButtonDown() && mouseEvent.getClickCount() == 2){
            FileRecord f = windowFilesTableView.getSelectionModel().getSelectedItem();

            if (!f.getLocalPath().exists()){
                showAlertDialog("Chyba","Soubor není přiložen");
                return;
            }

            openFile(f.getLocalPath());
        }
    }

    public static void openFile(final File openPath){
        try {
            if(!Desktop.isDesktopSupported()){
                showAlertDialog("Nepodařilo se otevřít soubor", "Platforma neumožňuje otevřít soubor z programu.");
                return;
            }

            Desktop desktop = Desktop.getDesktop();

            if(openPath.exists()) {
                desktop.open(openPath);
            }else {
                showAlertDialog("Soubor nebyl nalezen", String.format("Soubor %s není přiložen k reportu.", openPath.getPath()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlertDialog("Nepodařilo se otevřít soubor", "Při otevírání souboru nastala neočekávaná chyba.");
        }
    }

    @FXML
    public void showFulltextSearchWindow(ActionEvent actionEvent) throws IOException {
        if(isFilesIndexed) {
            Parent root = FXMLLoader.load(getClass().getResource("/reportEnchancer/view/fulltext-search.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Vyhledat v souborech");
            stage.setScene(new Scene(root, 500, 400));
            stage.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Hledat");
            alert.setHeaderText("Soubory nejsou zaindexovány k hledání, indexovat nyní?");

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                if(reportFilesDir == null) {
                    DirectoryChooser directoryChooser = new DirectoryChooser();
                    String userDirectoryString = System.getProperty("user.home") + "\\Desktop";
                    File userDirectory = new File(userDirectoryString);
                    directoryChooser.setInitialDirectory(userDirectory);
                    directoryChooser.setTitle("Zvolte složku s html soubory");
                    directoryChooser.setInitialDirectory(userDirectory);
                    reportFilesDir = directoryChooser.showDialog(windowBookmarksListView.getScene().getWindow());
                }

                if (reportFilesDir != null) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/IndexFiles.fxml"));
                    Parent root = loader.load();
                    IndexFilesController indexerController = loader.getController();
                    Stage indexStage = new Stage();
                    indexStage.setTitle("Indexace...");
                    indexStage.setScene(new Scene(root, 300, 100));
                    indexStage.show();
                    indexerController.start(reportFilesDir);
                    setFilesIndexed(true);
                }
            }
        }
    }

    public void showFileOverview(ActionEvent actionEvent) throws IOException{
        showBrowser(fileOverview);
    }

    public void showCaseInformation(ActionEvent actionEvent) throws IOException{
        showBrowser(caseInformation);
    }

    public void showEvidenceList(ActionEvent actionEvent) throws IOException{
        showBrowser(evidenceList);
    }

    private void showBrowser(String HTML) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/browser.fxml"));
        Parent root = loader.load();
        BrowserController controller = (BrowserController) loader.getController();
        controller.load(HTML);
        Stage stage = new Stage();
        stage.setTitle("Náhled");
        stage.setScene(new Scene(root, 600, 400));
        stage.show();
    }

    public void setCaseInformation(String caseInformation) {
        this.caseInformation = caseInformation;
    }

    public void setFileOverview(String fileOverview) {
        this.fileOverview = fileOverview;
    }

    public void setEvidenceList(String evidenceList) {
        this.evidenceList = evidenceList;
    }

    public void setReportFilesDir(File reportFilesDir) {
        this.reportFilesDir = reportFilesDir;
    }

    public void exporSelected(ActionEvent actionEvent) throws IOException{
        initExport(selectedFiles);
    }

    public void exportCategory(ActionEvent actionEvent) throws IOException{
        initExport(query.getFiles());
    }

    public void exportAll(ActionEvent actionEvent) throws IOException{
        Query qBackup = query.clone();
        query
                .reset()
                .limit(LIMIT);

        initExport(query.getFiles());
        query = qBackup;
    }

    private void initExport(ArrayList<FileRecord> list) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/export-wizard.fxml"));
        Parent root = loader.load();

        ExportWizardController controller = (ExportWizardController) loader.getController();
        controller.setCaseInformation(caseInformation);
        controller.setFileOverview(fileOverview);
        controller.setEvidenceList(evidenceList);
        controller.setColumns(new ArrayList<TableColumn<FileRecord, ?>>(windowFilesTableView.getColumns()));
        controller.setFileList(list);

        Stage stage = new Stage();
        stage.setTitle("Export");
        stage.setScene(new Scene(root, 300, 400));
        stage.show();
    }

    public void resetSelection(ActionEvent actionEvent) {
        syncFileTableWithDBManager();
        selectedFiles = new ArrayList<FileRecord>();
    }

    public void enterSubmit(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            runSearch();
        }
    }

    public void showDateRangeSearch(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/date-range-search.fxml"));
        Parent root = loader.load();

        DateRangeSearch controller = (DateRangeSearch) loader.getController();
        controller.submitListenner(this);

        Stage stage = new Stage();
        stage.setTitle("Vyhledat");
        stage.setScene(new Scene(root, 200, 200));
        stage.show();
    }

    @Override
    public void onSearchClicked(String column,LocalDate from, LocalDate to) {
        Query q = query
                .clone()
                .between(column, java.sql.Date.valueOf(from).getTime(), java.sql.Date.valueOf(to).getTime());
        windowFilesTableView.getItems().setAll(q.getFiles());
    }

    public void handleBookmarkSelected(MouseEvent mouseEvent) {
        String nextSelected = windowBookmarksListView.getSelectionModel().getSelectedItem().toString();

        if(nextSelected.equals(selectedBookmark)){
            windowBookmarksListView.getSelectionModel().clearSelection();
            query.removeWhere(Query.BOOKMARKS_ID);
        } else {

            int id = query
                    .clone()
                    .reset()
                    .limit(LIMIT)
                    .table(Query.TABLE_BOOKMARKS)
                    .where(Query.NAME, "=", nextSelected)
                    .getBookmarks()
                    .get(0)
                    .getKey();

            query
                    .where(Query.BOOKMARKS_ID, "=", id)
                    .orderBy(Query.NAME)
                    .order(Query.ORDER.DESC);
        }
        this.syncFileTableWithDBManager();
        selectedBookmark = nextSelected;
    }

    @FXML
    public void handleExtGroupSelected() {
        String nextSelected = windowExtensionsListView.getSelectionModel().getSelectedItem().toString();

        if(nextSelected.equals(selectedExtension)){
            windowExtensionsListView.getSelectionModel().clearSelection();
            query.removeWhere(Query.EXTENSIONS_ID);
        } else {

            int id = query
                    .clone()
                    .reset()
                    .table(Query.TABLE_EXTENSIONS)
                    .where(Query.NAME, "=", nextSelected)
                    .getExtensions()
                    .get(0)
                    .getKey();

            query
                    .where(Query.EXTENSIONS_ID, "=", id)
                    .orderBy(Query.NAME)
                    .order(Query.ORDER.DESC);
        }
        this.syncFileTableWithDBManager();
        selectedExtension = nextSelected;
    }

    public void ctxOpenFile(ActionEvent actionEvent) {
        FileRecord f = windowFilesTableView.getSelectionModel().getSelectedItem();

        if (!f.getLocalPath().exists()){
            showAlertDialog("Chyba","Soubor není přiložen");
            return;
        }

        openFile(f.getLocalPath());
    }

    public void ctxCheckSum(ActionEvent actionEvent) throws IOException {
        FileRecord f = windowFilesTableView.getSelectionModel().getSelectedItem();
        if (!f.getLocalPath().exists()){
            showAlertDialog("Chyba","Soubor není přiložen");
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/check-sum.fxml"));
        Parent root = loader.load();

        CheckSumController controller = (CheckSumController) loader.getController();
        controller.compare(f.getMD5(), f.getSHA1(), f.getLocalPath());

        Stage stage = new Stage();
        stage.setTitle("Kontrolní součet");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void generateChecksum(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/generate_checksum.fxml"));
        Parent root = loader.load();

        Stage stage = new Stage();
        stage.setTitle("Kontrolní součet");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void importCSV(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/reportEnchancer/view/csv_import.fxml"));
        Parent root = loader.load();
        Stage indexStage = new Stage();
        indexStage.setTitle("CSV import");
        indexStage.setScene(new Scene(root));
        indexStage.showAndWait();
        initBookmarksExtensions();
    }

    public void selectCurrent() {
        for (FileRecord fr : windowFilesTableView.getItems()){
            fr.setChecked(true);
        }
    }

    public void selectAll(ActionEvent actionEvent) {
        selectCurrent();
        ArrayList<FileRecord> list = this.query.clone().limit(-1).offset(0).getFiles();
        for (FileRecord fr : list) {
            if(selectedFiles.stream().noneMatch(f -> f.getID() == fr.getID())){
                selectedFiles.add(fr);
            }
        }
    }
}
