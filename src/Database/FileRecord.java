package Database;

import ReportParser.ParsedFile;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class FileRecord {
    private String name;
    private long physicalSize;
    private long logicalSize;
    private Date created;
    private Date modified;
    private Date accessed;
    private String path;
    private String exported;
    private String comment;
    private String label;
    private int itemNumber;
    private String ftkExtension;
    private String fileType;
    private String MD5;
    private String SHA1;
    private boolean carved;
    private boolean deleted;
    private String author;
    private String lastSavedBy;
    private Date emailSubmitTime;
    private Date emailDeliveryTime;
    private SimpleBooleanProperty checked = new SimpleBooleanProperty(false);
    private int ID;
    private Date createTime;
    private String embeddedComments;
    private String hiddenColumnsOrRows;
    private String hiddenWorksheets;
    private Date lastPrinted;
    private Date lastSavedTime;
    private int revisionNumber;
    private String totalEditingTime;
    private String trackChanges;
    private String emailSubject;
    private String emailTo;
    private String emailFrom;
    private String emailCC;
    private String emailBCC;
    private String emailUnread;
    private String emailUnsent;
    private String emailHasAttachment;
    private String emailPriority;
    private String email;
    private String emailSrc;
    private String category;

    public static ArrayList<String> numericColumns;
    public static HashMap<String, String> mapToDBCols;

    static {
        numericColumns = new ArrayList<String>();
        numericColumns.add(Query.PHYSICAL_SIZE);
        numericColumns.add(Query.LOGICAL_SIZE);
        numericColumns.add(Query.ITEM_NUMBER);
        numericColumns.add(Query.CARVED);
        numericColumns.add(Query.DELETED);
        numericColumns.add(Query.REVISION_NUMBER);
        numericColumns.add(Query.LAST_PRINTED);
        numericColumns.add(Query.LAST_SAVED_TIME);
        numericColumns.add(Query.CREATE_TIME);
        numericColumns.add(Query.EMAIL_DELIVERY_TIME);
        numericColumns.add(Query.EMAIL_SUBMIT_TIME);
        numericColumns.add(Query.CREATED_DATE);
        numericColumns.add(Query.UPDATED_DATE);
        numericColumns.add(Query.ACCESSED_DATE);

        mapToDBCols = new HashMap<String, String>();
        mapToDBCols.put("name", Query.NAME);
        mapToDBCols.put("physicalSize", Query.PHYSICAL_SIZE);
        mapToDBCols.put("logicalSize", Query.LOGICAL_SIZE);
        mapToDBCols.put("created", Query.CREATED_DATE);
        mapToDBCols.put("modified", Query.UPDATED_DATE);
        mapToDBCols.put("accessed", Query.ACCESSED_DATE);
        mapToDBCols.put("path", Query.PATH);
        mapToDBCols.put("exported", Query.EXPORTED);
        mapToDBCols.put("comment", Query.COMMENTS);
        mapToDBCols.put("label", Query.LABEL);
        mapToDBCols.put("itemNumber", Query.ITEM_NUMBER);
        mapToDBCols.put("ftkExtension", Query.EXTENSION);
        mapToDBCols.put("fileType", Query.FILE_TYPE);
        mapToDBCols.put("MD5", Query.MD5);
        mapToDBCols.put("SHA1", Query.SHA1);
        mapToDBCols.put("carved", Query.CARVED);
        mapToDBCols.put("deleted", Query.DELETED);
        mapToDBCols.put("author", Query.AUTHOR);
        mapToDBCols.put("lastSavedBy", Query.LAST_SAVED_BY);
        mapToDBCols.put("emailSubmitTime", Query.EMAIL_SUBMIT_TIME);
        mapToDBCols.put("emailDeliveryTime", Query.EMAIL_DELIVERY_TIME);
        mapToDBCols.put("createTime", Query.CREATE_TIME);
        mapToDBCols.put("embeddedComments", Query.EMBEDDED_COMMENTS);
        mapToDBCols.put("hiddenColumnsOrRows", Query.HIDDEN_COLUMNS_OR_ROWS);
        mapToDBCols.put("hiddenWorksheets", Query.HIDDEN_WORKSHEETS);
        mapToDBCols.put("lastPrinted", Query.LAST_PRINTED);
        mapToDBCols.put("lastSavedTime", Query.LAST_SAVED_TIME);
        mapToDBCols.put("private int revisionNumber", Query.REVISION_NUMBER);
        mapToDBCols.put("totalEditingTime", Query.TOTAL_EDITING_TIME);
        mapToDBCols.put("trackChanges", Query.TRACK_CHANGES);
        mapToDBCols.put("emailSubject", Query.EMAIL_SUBJECT);
        mapToDBCols.put("emailTo", Query.EMAIL_TO);
        mapToDBCols.put("emailFrom", Query.EMAIL_FROM);
        mapToDBCols.put("emailCC", Query.EMAIL_CC);
        mapToDBCols.put("emailBCC", Query.EMAIL_BCC);
        mapToDBCols.put("emailUnread", Query.EMAIL_UNREAD);
        mapToDBCols.put("emailUnsent", Query.EMAIL_UNSENT);
        mapToDBCols.put("emailHasAttachment", Query.EMAIL_HAS_ATTACHMENT);
        mapToDBCols.put("emailPriority", Query.EMAIL_PRIORITY);
        mapToDBCols.put("email", Query.EMAIL);
        mapToDBCols.put("emailSrc", Query.EMAIL_SRC);
        mapToDBCols.put("category", Query.CATEGORY);
    }

    /**
     *
     * @param name
     * @param physicalSize
     * @param logicalSize
     * @param created
     * @param modified
     * @param accessed
     * @param path
     * @param exported
     * @param comment
     * @param label
     * @param itemNumber
     * @param ftkExtension
     * @param fileType
     * @param MD5
     * @param SHA1
     * @param carved
     * @param deleted
     * @param author
     * @param lastSavedBy
     * @param emailSubmitTime
     * @param emailDeliveryTime
     * @param ID
     * @param createTime
     * @param embeddedComments
     * @param hiddenColumnsOrRows
     * @param hiddenWorksheets
     * @param lastPrinted
     * @param lastSavedTime
     * @param revisionNumber
     * @param totalEditingTime
     * @param trackChanges
     * @param emailSubject
     * @param emailTo
     * @param emailFrom
     * @param emailCC
     * @param emailBCC
     * @param emailUnread
     * @param emailUnsent
     * @param emailHasAttachment
     * @param emailPriority
     * @param email
     * @param emailSrc
     * @param category
     */
    public FileRecord(String name, long physicalSize, long logicalSize, Date created, Date modified, Date accessed, String path, String exported, String comment, String label, int itemNumber, String ftkExtension, String fileType, String MD5, String SHA1, boolean carved, boolean deleted, String author, String lastSavedBy, Date emailSubmitTime, Date emailDeliveryTime, int ID, Date createTime, String embeddedComments, String hiddenColumnsOrRows, String hiddenWorksheets, Date lastPrinted, Date lastSavedTime, int revisionNumber, String totalEditingTime, String trackChanges, String emailSubject, String emailTo, String emailFrom, String emailCC, String emailBCC, String emailUnread, String emailUnsent, String emailHasAttachment, String emailPriority, String email, String emailSrc, String category) {
        this.name = name;
        this.physicalSize = physicalSize;
        this.logicalSize = logicalSize;
        this.created = created;
        this.modified = modified;
        this.accessed = accessed;
        this.path = path;
        this.exported = exported;
        this.comment = comment;
        this.label = label;
        this.itemNumber = itemNumber;
        this.ftkExtension = ftkExtension;
        this.fileType = fileType;
        this.MD5 = MD5;
        this.SHA1 = SHA1;
        this.carved = carved;
        this.deleted = deleted;
        this.author = author;
        this.lastSavedBy = lastSavedBy;
        this.emailSubmitTime = emailSubmitTime;
        this.emailDeliveryTime = emailDeliveryTime;
        this.ID = ID;
        this.createTime = createTime;
        this.embeddedComments = embeddedComments;
        this.hiddenColumnsOrRows = hiddenColumnsOrRows;
        this.hiddenWorksheets = hiddenWorksheets;
        this.lastPrinted = lastPrinted;
        this.lastSavedTime = lastSavedTime;
        this.revisionNumber = revisionNumber;
        this.totalEditingTime = totalEditingTime;
        this.trackChanges = trackChanges;
        this.emailSubject = emailSubject;
        this.emailTo = emailTo;
        this.emailFrom = emailFrom;
        this.emailCC = emailCC;
        this.emailBCC = emailBCC;
        this.emailUnread = emailUnread;
        this.emailUnsent = emailUnsent;
        this.emailHasAttachment = emailHasAttachment;
        this.emailPriority = emailPriority;
        this.email = email;
        this.emailSrc = emailSrc;
        this.category = category;
    }

    /**
     *
     * @return
     */
    public String getExtension(){
        int pos = name.lastIndexOf('.');
        if(pos != -1){
            String afterDot = name.substring(pos +1);
            if(afterDot.length() < 5){
                return afterDot;
            }
        }
        return ParsedFile.NO_EXTENSION;
    }

    /**
     *
     * @return
     */
    public boolean isImage() {
        return getExtension().equals("jpg")
                || getExtension().equals("jpeg")
                || getExtension().equals("png")
                || getExtension().equals("gif");
    }

    /**
     *
     * @return
     */
    public File getLocalPath(){
        String q = String.format("SELECT * FROM %s WHERE %s = \"%s\"", Query.TABLE_FILE_PATHS, Query.NAME, name);
        System.out.println(q);
        try (Connection conn = Query.getDbConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(q)) {
            return new File(rs.getString(Query.LOCAL_PATH));
        } catch (SQLException e){
            e.printStackTrace();
            return new File("");
        }
    }

    /**
     *
     * @return
     */
    public boolean isEmail(){
        return getExtension().equals("eml");
    }

    /**
     *
     * @return
     */
    public boolean isText(){
        return getExtension().equals("txt");
    }

    /**
     *
     * @return
     */
    public SimpleBooleanProperty checkedProperty() {
        return this.checked;
    }


    /**
     *
     * @return
     */
    public java.lang.Boolean getChecked() {
        return this.checkedProperty().get();
    }


    /**
     *
     * @param checked
     */
    public void setChecked(final java.lang.Boolean checked) {
        this.checkedProperty().set(checked);
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return
     */
    public long getPhysicalSize() {
        return physicalSize;
    }

    /**
     *
     * @return
     */
    public long getLogicalSize() {
        return logicalSize;
    }

    /**
     *
     * @return
     */
    public Date getCreated() {
        return created;
    }

    /**
     *
     * @return
     */
    public int getID() {
        return ID;
    }

    /**
     *
     * @return
     */
    public Date getModified() {
        return modified;
    }

    /**
     *
     * @return
     */
    public Date getAccessed() {
        return accessed;
    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @return
     */
    public String getExported() {
        return exported;
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @return
     */
    public int getItemNumber() {
        return itemNumber;
    }

    /**
     *
     * @return
     */
    public String getFtkExtension() {
        return ftkExtension;
    }

    /**
     *
     * @return
     */
    public String getFileType() {
        return fileType;
    }

    /**
     *
     * @return
     */
    public String getMD5() {
        return MD5;
    }

    /**
     *
     * @return
     */
    public String getSHA1() {
        return SHA1;
    }

    /**
     *
     * @return
     */
    public boolean isCarved() {
        return carved;
    }

    /**
     *
     * @return
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     *
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @return
     */
    public String getLastSavedBy() {
        return lastSavedBy;
    }

    /**
     *
     * @return
     */
    public Date getEmailSubmitTime() {
        return emailSubmitTime;
    }

    /**
     *
     * @return
     */
    public Date getEmailDeliveryTime() {
        return emailDeliveryTime;
    }

    /**
     *
     * @return
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     *
     * @return
     */
    public String getEmbeddedComments() {
        return embeddedComments;
    }

    /**
     *
     * @return
     */
    public String getHiddenColumnsOrRows() {
        return hiddenColumnsOrRows;
    }

    /**
     *
     * @return
     */
    public String getHiddenWorksheets() {
        return hiddenWorksheets;
    }

    /**
     *
     * @return
     */
    public Date getLastPrinted() {
        return lastPrinted;
    }

    /**
     *
     * @return
     */
    public Date getLastSavedTime() {
        return lastSavedTime;
    }

    /**
     *
     * @return
     */
    public int getRevisionNumber() {
        return revisionNumber;
    }

    /**
     *
     * @return
     */
    public String getTotalEditingTime() {
        return totalEditingTime;
    }

    /**
     *
     * @return
     */
    public String getTrackChanges() {
        return trackChanges;
    }

    /**
     *
     * @return
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     *
     * @return
     */
    public String getEmailTo() {
        return emailTo;
    }

    /**
     *
     * @return
     */
    public String getEmailFrom() {
        return emailFrom;
    }

    /**
     *
     * @return
     */
    public String getEmailCC() {
        return emailCC;
    }

    /**
     *
     * @return
     */
    public String getEmailBCC() {
        return emailBCC;
    }

    /**
     *
     * @return
     */
    public String getEmailUnread() {
        return emailUnread;
    }

    /**
     *
     * @return
     */
    public String getEmailUnsent() {
        return emailUnsent;
    }

    /**
     *
     * @return
     */
    public String getEmailHasAttachment() {
        return emailHasAttachment;
    }

    /**
     *
     * @return
     */
    public String getEmailPriority() {
        return emailPriority;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @return
     */
    public String getEmailSrc() {
        return emailSrc;
    }

    /**
     *
     * @return
     */
    public String getCategory() {
        return category;
    }
}
