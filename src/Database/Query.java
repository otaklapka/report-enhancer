package Database;

import javafx.util.Pair;
import reportEnchancer.model.EnhancerDate;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Query {

    public static final String TABLE_FILES = "FILES";
    public static final String TABLE_BOOKMARKS = "BOOKMARKS";
    public static final String TABLE_EXTENSIONS = "EXTENSIONS";
    public static final String TABLE_FILE_PATHS = "FILE_PATHS";


    public static final String LOCAL_PATH = "local_path";
    public static final String ID = "id";
    public static final String COMMENTS = "comments";
    public static final String NAME = "name";
    public static final String EXTENSION = "extension";
    public static final String EXTENSIONS_ID = "extensions_id";
    public static final String BOOKMARKS_ID = "bookmarks_id";
    public static final String PHYSICAL_SIZE = "physical_size";
    public static final String LOGICAL_SIZE = "logical_size";
    public static final String CREATED_DATE = "created_date";
    public static final String UPDATED_DATE = "updated_date";
    public static final String EMAIL_SUBMIT_TIME = "email_submit_time";
    public static final String EMAIL_DELIVERY_TIME = "email_delivery_time";
    public static final String ACCESSED_DATE = "accessed_date";
    public static final String LABEL = "label";
    public static final String ITEM_NUMBER = "item_number";
    public static final String FILE_TYPE = "file_type";
    public static final String MD5 = "MD5";
    public static final String SHA1 = "SHA1";
    public static final String CARVED = "carved";
    public static final String DELETED = "deleted";
    public static final String AUTHOR = "author";
    public static final String LAST_SAVED_BY = "last_saved_by";
    public static final String PATH = "path";
    public static final String EXPORTED = "exported";
    public static final String CREATE_TIME = "create_time";
    public static final String EMBEDDED_COMMENTS = "embedded_comments";
    public static final String HIDDEN_COLUMNS_OR_ROWS = "hidden_columns_or_rows";
    public static final String HIDDEN_WORKSHEETS = "hidden_worksheets";
    public static final String LAST_PRINTED = "last_printed";
    public static final String LAST_SAVED_TIME = "last_saved_time";
    public static final String REVISION_NUMBER = "revision_number";
    public static final String TOTAL_EDITING_TIME = "total_editing_time";
    public static final String TRACK_CHANGES = "track_changes";
    public static final String EMAIL_SUBJECT = "subject";
    public static final String EMAIL_TO = "to";
    public static final String EMAIL_FROM = "from";
    public static final String EMAIL_CC = "cc";
    public static final String EMAIL_BCC = "bcc";
    public static final String EMAIL_UNREAD = "unread";
    public static final String EMAIL_UNSENT = "unsent";
    public static final String EMAIL_HAS_ATTACHMENT = "has_attachment";
    public static final String EMAIL_PRIORITY = "priority";
    public static final String EMAIL = "email";
    public static final String EMAIL_SRC = "src";
    public static final String CATEGORY = "category";
    public static final String CON_STR = "jdbc:sqlite:";
    public static String CON_URL = "database.sqlite3";
    public enum ORDER {ASC, DESC}

    ;

    private HashMap<String, String[]> _where;
    private String _orderBy;
    private ORDER _order;
    private int _limit;
    private String _table;
    private int _offset;
    private Pair _like;
    private Pair<String, String[]> _between;
    public Query() {
        reset();
    }

    /**
     *
     * @return Query
     */
    public Query clone() {
        return new Query(
                this._where,
                this._orderBy,
                this._order,
                this._limit,
                this._table,
                this._offset,
                this._like,
                this._between
        );
    }

    /**
     *
     * @param _where
     * @param _orderBy
     * @param _order
     * @param _limit
     * @param _table
     * @param _offset
     * @param _like
     * @param _between
     */
    public Query(HashMap<String, String[]> _where, String _orderBy, ORDER _order, int _limit, String _table, int _offset, Pair _like, Pair _between) {
        this._where = _where;
        this._orderBy = _orderBy;
        this._order = _order;
        this._limit = _limit;
        this._table = _table;
        this._offset = _offset;
        this._like = _like;
        this._between = _between;
    }

    /**
     *
     * @return
     */
    public Query reset() {
        _where = new HashMap<String, String[]>();
        _orderBy = ID;
        _order = ORDER.DESC;
        _limit = -1;
        _table = TABLE_FILES;
        _offset = 0;
        _like = null;
        _between = null;
        return this;
    }

    /**
     *
     * @param table
     * @return
     */
    public Query table(String table) {
        this._table = table;
        return this;
    }

    /**
     *
     * @param column
     * @return
     */
    public Query removeWhere(String column) {
        _where.remove(column);
        return this;
    }

    /**
     *
     * @param offset
     * @return
     */
    public Query offset(int offset) {
        this._offset = offset;
        return this;
    }

    /**
     *
     * @param column
     * @param min
     * @param max
     * @return
     */
    public Query between(String column, long min, long max) {
        String[] x = {Long.toString(min), Long.toString(max)};
        _between = new Pair(column, x);
        return this;
    }

    /**
     *
     * @param column
     * @param min
     * @param max
     * @return
     */
    public Query between(String column, String min, String max) {
        String[] x = {String.format("\"%s\"", min), String.format("\"%s\"", max)};
        _between = new Pair(column, x);
        return this;
    }

    /**
     *
     * @param column
     * @param pattern
     * @return
     */
    public Query like(String column, String pattern) {
        _like = new Pair(column, pattern);
        return this;
    }

    /**
     *
     * @param column
     * @param operator
     * @param value
     * @return
     */
    public Query where(String column, String operator, String value) {
        String[] s = {operator, String.format("\"%s\"", value)};
        _where.put(column, s);
        return this;
    }

    /**
     *
     * @param column
     * @param operator
     * @param value
     * @return
     */
    public Query where(String column, String operator, long value) {
        String[] s = {operator, Long.toString(value)};
        _where.put(column, s);
        return this;
    }

    /**
     *
     * @param column
     * @param operator
     * @param value
     * @return
     */
    public Query where(String column, String operator, boolean value) {
        String[] s = {operator, String.format("\"%s\"", value)};
        _where.put(column, s);
        return this;
    }

    /**
     *
     * @param column
     * @return
     */
    public Query orderBy(String column) {
        _orderBy = column;
        return this;
    }

    /**
     *
     * @param limit
     * @return
     */
    public Query limit(int limit) {
        _limit = limit;
        return this;
    }

    /**
     *
     * @param order
     * @return
     */
    public Query order(ORDER order) {
        _order = order;
        return this;
    }

    private String buildQuery() {
        return buildQuery("*");
    }

    private String buildQuery(String select) {
        String cmd = "WHERE";
        String andQ = "";
        for (Map.Entry<String, String[]> entry : _where.entrySet()) {
            andQ += String.format("%s `%s` %s %s ", cmd, entry.getKey(), entry.getValue()[0], entry.getValue()[1]);
            cmd = "AND";
        }

        String order = _order == ORDER.DESC ? "DESC" : "ASC";
        String limit = String.format("LIMIT %d", _limit);
        String offset = String.format("OFFSET %d", _offset);
        String orderBy = String.format("ORDER BY %s", _orderBy);
        String like = _like != null ? String.format("%s %s LIKE \"%%%s%%\" ", andQ.length() == 0 ? "WHERE" : "AND", _like.getKey(), _like.getValue()) : "";
        String between = _between != null ? String.format("%s %s BETWEEN %s AND %s ", like.length() == 0 && andQ.length() == 0 ? "WHERE" : "AND", _between.getKey(), _between.getValue()[0], _between.getValue()[1]) : "";

        String q = String.format("SELECT %s FROM %s %s %s %s %s %s %s %s", select, _table, andQ, like, between, orderBy, order, limit, offset);
        System.out.println(q);
        return q;
    }

    /**
     *
     * @return
     */
    public ArrayList<Pair<Integer, String>> getBookmarks() {
        ArrayList list = new ArrayList();
        try (Connection conn = getDbConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(buildQuery())) {

            while (rs.next()) {
                list.add(new Pair<Integer, String>(rs.getInt(ID), rs.getString(NAME)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *
     * @return
     */
    public ArrayList<Pair<Integer, String>> getExtensions() {
        return getBookmarks();
    }

    /**
     *
     * @return
     */
    public ArrayList<FileRecord> getFiles() {
        ArrayList<FileRecord> list = new ArrayList<FileRecord>();
        try (Connection conn = getDbConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(buildQuery())) {

            while (rs.next()) {
                FileRecord fr = new FileRecord(
                        rs.getString(NAME),
                        rs.getLong(PHYSICAL_SIZE),
                        rs.getLong(LOGICAL_SIZE),
                        new EnhancerDate(rs.getDate(CREATED_DATE).getTime()),
                        new EnhancerDate(rs.getDate(UPDATED_DATE).getTime()),
                        new EnhancerDate(rs.getDate(ACCESSED_DATE).getTime()),
                        rs.getString(PATH),
                        rs.getString(EXPORTED),
                        rs.getString(COMMENTS),
                        rs.getString(LABEL),
                        rs.getInt(ITEM_NUMBER),
                        rs.getString(EXTENSION),
                        rs.getString(FILE_TYPE),
                        rs.getString(MD5),
                        rs.getString(SHA1),
                        rs.getBoolean(CARVED),
                        rs.getBoolean(DELETED),
                        rs.getString(AUTHOR),
                        rs.getString(LAST_SAVED_BY),
                        new EnhancerDate(rs.getDate(EMAIL_SUBMIT_TIME).getTime()),
                        new EnhancerDate(rs.getDate(EMAIL_DELIVERY_TIME).getTime()),
                        rs.getInt(ID),
                        new EnhancerDate(rs.getDate(CREATE_TIME).getTime()),
                        rs.getString(EMBEDDED_COMMENTS),
                        rs.getString(HIDDEN_COLUMNS_OR_ROWS),
                        rs.getString(HIDDEN_WORKSHEETS),
                        new EnhancerDate(rs.getDate(LAST_PRINTED).getTime()),
                        new EnhancerDate(rs.getDate(LAST_SAVED_TIME).getTime()),
                        rs.getInt(REVISION_NUMBER),
                        rs.getString(TOTAL_EDITING_TIME),
                        rs.getString(TRACK_CHANGES),
                        rs.getString(EMAIL_SUBJECT),
                        rs.getString(EMAIL_TO),
                        rs.getString(EMAIL_FROM),
                        rs.getString(EMAIL_CC),
                        rs.getString(EMAIL_BCC),
                        rs.getString(EMAIL_UNREAD),
                        rs.getString(EMAIL_UNSENT),
                        rs.getString(EMAIL_HAS_ATTACHMENT),
                        rs.getString(EMAIL_PRIORITY),
                        rs.getString(EMAIL),
                        rs.getString(EMAIL_SRC),
                        rs.getString(CATEGORY)
                );

                list.add(fr);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *
     * @return
     */
    public int getCount() {
        final String count = "count";
        String q = buildQuery(String.format("COUNT(%s) as %s", Query.ID, count));
        System.out.println(q);
        try (Connection conn = getDbConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(q)) {
            return rs.getInt(count);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     *
     * @return
     */
    public static Connection getDbConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(String.format("%s%s", CON_STR, CON_URL));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    /**
     *
     * @return
     */
    public HashMap<String, String[]> get_where() {
        return _where;
    }

    /**
     *
     * @return
     */
    public String getOrderBy() {
        return _orderBy;
    }

    /**
     *
     * @return
     */
    public ORDER getOrder() {
        return _order;
    }

    /**
     *
     * @return
     */
    public int getLimit() {
        return _limit;
    }

    /**
     *
     * @return
     */
    public String getTable() {
        return _table;
    }

    /**
     *
     * @return
     */
    public int getOffset() {
        return _offset;
    }

    /**
     *
     * @return
     */
    public Pair getLike() {
        return _like;
    }
}
