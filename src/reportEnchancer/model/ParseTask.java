package reportEnchancer.model;

import javafx.concurrent.Task;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static Database.Query.*;

public class ParseTask extends Task<Void> {
    public static String INSERT_FILE_QUERY = "INSERT INTO FILES VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static String INSERT_EXTENSION_QUERY = "INSERT INTO EXTENSIONS VALUES (?, ?)";
    public static String INSERT_BOOKMARK_QUERY = "INSERT INTO BOOKMARKS VALUES (?, ?)";
    public static String INSERT_FILE_PATHS_QUERY = "INSERT INTO FILE_PATHS VALUES (NULL, ?, ?)";
    private  static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    public static int parseInt(String number){
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static Date parseDate(String date){
        try {
            return new Date(dateFormat.parse(date).getTime());
        } catch (ParseException ex) {
            ex.printStackTrace();
            return new Date(0);
        }
    }

    @Override
    protected Void call() throws Exception {
        return null;
    }

    protected void prepareDBStructure() throws SQLException {
        updateMessage("Vytváření databázové struktury.");
        ArrayList<String> queries = new ArrayList<String>();
        queries.add(String.format("DROP TABLE IF EXISTS `%s`", TABLE_FILES));
        queries.add(String.format("DROP TABLE IF EXISTS `%s`", TABLE_EXTENSIONS));
        queries.add(String.format("DROP TABLE IF EXISTS `%s`", TABLE_BOOKMARKS));
        queries.add(String.format("DROP TABLE IF EXISTS `%s`", TABLE_FILE_PATHS));

        queries.add(String.format("CREATE TABLE `%s` (`%s` INTEGER PRIMARY KEY AUTOINCREMENT, `%s` TEXT)", TABLE_EXTENSIONS, ID, NAME));
        queries.add(String.format("CREATE TABLE `%s` (`%s` INTEGER PRIMARY KEY AUTOINCREMENT, `%s` TEXT)", TABLE_BOOKMARKS, ID, NAME));
        queries.add(String.format("CREATE TABLE `%s` (`%s` INTEGER PRIMARY KEY AUTOINCREMENT, `%s` TEXT, `%s` TEXT)", TABLE_FILE_PATHS, ID, NAME, LOCAL_PATH));
        String fq = String.format("CREATE TABLE `%s` (" +
                        " `%s` INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` INTEGER," +
                        " `%s` INTEGER," +
                        " `%s` INTEGER," +
                        " `%s` INTEGER," +
                        " `%s` DATE," +
                        " `%s` DATE," +
                        " `%s` DATE," +
                        " `%s` DATE," +
                        " `%s` DATE," +
                        " `%s` TEXT," +
                        " `%s` INTEGER," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` INTEGER," +
                        " `%s` INTEGER," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +

                        " `%s` DATE," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` DATE," +
                        " `%s` DATE," +
                        " `%s` INTEGER," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +
                        " `%s` TEXT," +

                        " FOREIGN KEY(%s) REFERENCES `%s`(id)," +
                        " FOREIGN KEY(%s) REFERENCES `%s`(id)" +
                        ")",
                TABLE_FILES,
                ID,
                COMMENTS,
                NAME,
                EXTENSION,
                EXTENSIONS_ID,
                BOOKMARKS_ID,
                PHYSICAL_SIZE,
                LOGICAL_SIZE,
                CREATED_DATE,
                UPDATED_DATE,
                EMAIL_SUBMIT_TIME,
                EMAIL_DELIVERY_TIME,
                ACCESSED_DATE, LABEL,
                ITEM_NUMBER,
                FILE_TYPE,
                MD5,
                SHA1,
                CARVED,
                DELETED,
                AUTHOR,
                LAST_SAVED_BY,
                PATH,
                EXPORTED,

                CREATE_TIME,
                EMBEDDED_COMMENTS,
                HIDDEN_COLUMNS_OR_ROWS,
                HIDDEN_WORKSHEETS,
                LAST_PRINTED,
                LAST_SAVED_TIME,
                REVISION_NUMBER,
                TOTAL_EDITING_TIME,
                TRACK_CHANGES, //32
                EMAIL_SUBJECT,
                EMAIL_TO,
                EMAIL_FROM,
                EMAIL_CC,
                EMAIL_BCC,
                EMAIL_UNREAD,
                EMAIL_UNSENT,
                EMAIL_HAS_ATTACHMENT,
                EMAIL_PRIORITY,
                EMAIL,
                EMAIL_SRC,
                CATEGORY,

                BOOKMARKS_ID,
                TABLE_BOOKMARKS,
                EXTENSIONS_ID,
                TABLE_EXTENSIONS
        );
        System.out.println(fq);
        queries.add(fq);


        Statement stmt = null;
        Connection dbConnection = this.getDbConnection();
        if (dbConnection != null) {
            try {
                stmt = dbConnection.createStatement();
                for (String query : queries) {
                    stmt.execute(query);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
                dbConnection.close();
            }
        }
    }

    protected Connection getDbConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:database.sqlite3");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
