package reportEnchancer.controller;

import fullTextSearch.LuceneConstants;
import fullTextSearch.Searcher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import reportEnchancer.model.Settings;

import java.io.File;

public class FulltextSearchController {
    @FXML
    private ListView resultsListView;
    @FXML
    private TextField searchValueTextField;

    @FXML
    public void fulltextSearch(ActionEvent actionEvent) {
        runSearch();
    }
    @FXML
    public void ListViewItemClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2){
            String file = resultsListView.getSelectionModel().getSelectedItem().toString();
            MainController.openFile(new File(file));
        }
    }

    public void enterSubmit(KeyEvent keyEvent) {
        runSearch();
    }

    private void runSearch(){
        String searchValue = searchValueTextField.getText();
        if(searchValue.length() == 0){
            return;
        }

        ObservableList<String> items = FXCollections.observableArrayList();
        try {
            Searcher searcher = new Searcher(Settings.prefs.get(Settings.INDEX_DIR, LuceneConstants.INDEX_FOLDER));
            TopDocs hits = searcher.search(searchValue);
            for (ScoreDoc scoreDoc : hits.scoreDocs) {
                Document doc = searcher.getDocument(scoreDoc);
                items.add(doc.get(LuceneConstants.FILE_PATH));
            }
            this.resultsListView.setItems(items);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
