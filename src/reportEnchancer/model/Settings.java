package reportEnchancer.model;

import fullTextSearch.LuceneConstants;

import java.util.prefs.Preferences;

public class Settings {
    public static final String INDEX_DIR = "index_dir";
    public static Preferences prefs;
    static {
        prefs = Preferences.userRoot().node(Settings.class.getName());
        prefs.put(INDEX_DIR, LuceneConstants.INDEX_FOLDER);
    }
}
